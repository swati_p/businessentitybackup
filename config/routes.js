/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': {
    view: 'homepage'
  },

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

  //API to add/update/delete/get business entity
  'post /addBusinessEntity/v1':'BusinessEntityMasterController.addBusinessEntity',
  'post /updateBusinessEntity/v1':'BusinessEntityMasterController.updateBusinessEntity',
  'post /deleteBusinessEntity/v1':'BusinessEntityMasterController.deleteBusinessEntity',
  'post /getBusinessEntity/v1':'BusinessEntityMasterController.getBusinessEntity',

  //API to add/update/delete/get business entity user
  'post /addBusinessEntityUser/v1':'BusinessEntityUsersMasterController.addBusinessEntityUser',
  'post /updateBusinessEntityUser/v1':'BusinessEntityUsersMasterController.updateBusinessEntityUser',
  'post /findBusinessEntityUser/v1':'BusinessEntityUsersMasterController.findBusinessEntityUser',
  'post /getBusinessEntityUsers/v1':'BusinessEntityUsersMasterController.getBusinessEntityUsers',


  //API to Bulk Upload Business Entity
  'post /businessEntityBulkUpload/v1':'BusinessEntityBulkUploadController.addBusinessEntityBulk',


  //API to get existing business entity and business entity user.
  

  //API to map/unmap programs to BusinessEntities
  'post /updateProgramsMapped/v1':'BusinessEntityMasterController.updateProgramsMapped',
   
  //API to check if users exists in Staging table while login from channel web app
  'post /loginBusinessEntity/v1':'BusinessEntityUsersMasterController.loginBusinessEntity',



  'get /resetApplicationConfigurations' : 'BusinessEntityMasterController.resetApplicationConfigurations' 

};
