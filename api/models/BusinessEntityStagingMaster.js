/**
 * BusinessEntityStagingMaster.js
 *
 * @description     :: To add, update and delete the business entities.
 * @createdBy       :: Nilesh
 * @created Date    :: 30-12-2016
 * @Last edited by  :: Ajeet
 * @last edited date::
 */


/**** Status code

    0  => success
    -1 => Error
    1  => Authentication failed
    2  => Data not found
    3  => Duplicate value exist.

****/

"use strict";

//Import required module.
var promise = require('promise');
var async = require('async');

module.exports = {

	//Table name and it attributes
  	tableName:'BusinessEntityStagingMaster',
	attributes:{
		businessId:{
			type:'string',
			primaryKey:true,
			unique:true,
			required:true
		},
		businessName:{
			type:'string',
			required:true
		},
		state:{
			type:'string',
			required:true
		},
		city:{
			type:'string',
			required:true
		},
		streetAddress:{
			type:'string',
			required:true
		},
		pincode:{
			type:'integer',
			required:true
		},
		landmark:{
			type:'string'
		},
		area:{
			type:'string'
		},
		yearOfEstablishment:{
			type:'integer'
//			required:true
		},
		dealsWithBrands:{
			type:'array',
		},
		registrationNumber:{
			type:'string'
//			required:true
		},
		/*----added later----*/
		gvStatus:{
			type:'boolean',
			defaultsTo:'false'
		},
		kycVerified:{
			type:'boolean',
			defaultsTo:'false'
		},
		/*-----------------*/
		kycStatus:{
			type:'boolean',
			defaultsTo:false
		},
		starPointEarned:{
			type:'integer',
			defaultsTo:0
		},
		dataSource:{
			type:'string',
			required:true
		},
		monthlyTurnOver:{
			type:'string'
		},
		approxShopSize:{
			type:'string'
		},
		// fields added later
		weeklyOff:{
			type:'array'
		},
		registrationProof:{
			type:'string'
		},
		// ---end
		programsInterested:{
			type:'array'
		},
		isStaging:{
			type:'boolean',
			defaultsTo:true
		},
		isDeleted:{
			type:'boolean',
			defaultsTo:false
		},
		doesUserAlreadyExist:{
			type:'boolean',
			defaultsTo:false
		},
		createdBy:{
			type:'string'
		},
		updatedBy:{
			type:'string'
		},
	},

//----------------------------------------------------------------------------------------------------------//

	//Function to add business entity
	addBusinessEntity:function(reqBody,token,next){

		//Variable declaration
		var businessEntityDetails = reqBody.businessEntityDetails; // To store business entity details
		var businessEntityUsers = reqBody.businessEntityUsers;// To store business entity user details
		var responseObj = {};// Response object is created.
		var businessId = null; // To store business entity id.

		// Response object with default value.
		responseObj = {
			statusCode:-1,
			message:null
		}


	// For adding logs -------------------------------------------------------//

		//variable defined to form requestApi
        var requestApi  = ServConfigService.getApplicationConfig().base_url
                        + ":"
                        + ServConfigService.getApplicationAPIs().addBusinessEntity.port
                        + ServConfigService.getApplicationAPIs().addBusinessEntity.url


        //Variable defined to create dataLogObject
        var dataLogObject = {
            "userId":reqBody.frontendUserInfo.userId,
            "requestApi":requestApi,
            "eventType":"add",
            "collection":"BusinessEntityStagingMaster"
        }
    //-----------------------------------------------------------------------//

		

		//To generate the business entity id.
		if (businessEntityDetails.businessId === undefined){
			var timeStamp = new Date().getTime();
			businessId = "BE"+timeStamp;
			businessEntityDetails.businessId = businessId;
		}
		else{			//storing business id in case of bulk upload
			businessId = businessEntityDetails.businessId;
		}	


		//To add KYC field and business id to business entity details object.
		businessEntityDetails.kycStatus = false;
		businessEntityDetails.isStaging = true;

	

		// to append business id to each user
		var i;
		var mobileNumberArray = [];     // array of mobile numbers to find
		for(i=0;i<businessEntityUsers.length;i++){
			businessEntityUsers[i].businessId = [];		//array of business ids field
			businessEntityUsers[i].businessId.push(businessId);	
			mobileNumberArray.push(businessEntityUsers[i].mobileNumber);    
		};
		
		// find object for findBusinessEntityUser
        var findObject = {
            kycStatus:true,
            mobileNumberArray:mobileNumberArray
        };


        BusinessEntityUserMaster.findBusinessEntityUser(findObject,function(response){
        	if(response.statusCode === 0){
                //append alreadyExist flag for duplicate mobileNumber
                businessEntityDetails.doesUserAlreadyExist = true;
                businessEntityDetails.existingBusinessEntityUsers = response.result;
            }
            else if(response.statusCode === -1){
                responseObj.message = response.error;
                return next(responseObj)
            }

			//To add business entity
			var addBusinessEntityPromise = new promise(function(resolve,reject){

				BusinessEntityStagingMaster.create(businessEntityDetails).exec(function(error,records){
					if(error || records === undefined){
						responseObj.message = error;
						sails.log.error("BusinessEntityStagingMasterModel>addBusinessEntity>BusinessEntityStagingMaster.create ","req body:"+ businessEntityDetails," error:"+error);
						reject();
					}else{
						responseObj.result = records;
						dataLogObject.newValue = records;
	                    LogService.addDataLog(dataLogObject,token);			//adding data log
						resolve();
					}
				})

			});

			addBusinessEntityPromise.then(function(){
				console.log("add entity success");
				console.log("token is",token);
				//To add business entity user call addBusinessEntityUser of BusinessEntityUserStagingMaster.
				BusinessEntityUserStagingMaster.addBusinessEntityUser(businessEntityUsers,token,function(res){

					//If user is added sucessfully then send appropriate message in response.
				 	if(res.statusCode === 0){
				 		responseObj.message = "Business entity and user has been added successfully to staging table";
						responseObj.statusCode = 0;
						responseObj.result.businessEntityUsers = res.result;	     // append the user object
						sails.log.info("BusinessEntityStagingMasterModel>addBusinessEntity>BusinessEntityStagingMaster.create: Business entity and user has been added successfully to staging table");
						//return next(responseObj);
				 	}else{

				 		//If user is not added then distroy the current bussiness entity.
				 		BusinessEntityStagingMaster.destroy({"businessId":businessId}).exec(function(error){
							if(error){
								console.log("unable to destroy record");
								responseObj.statusCode = -2;
								responseObj.message = "Contact Admin : Business entity created but failed to create user";
								responseObj.result = businessId;
								sails.log.error("BusinessEntityStagingMasterModel>addBusinessEntity>BusinessEntityStagingMaster.destroy ","req body:"+businessEntityDetails," error:"+error);
								//return next(responseObj);
							}else{
								responseObj.message = "Unable to Insert record";
								sails.log.info("BusinessEntityStagingMasterModel>addBusinessEntity>BusinessEntityStagingMaster.destroy: record is successfully destroyed");
								//return next(responseObj);
							}
						});
				 	}
				 	
					return next(responseObj);
				});

			},function(){
				console.log("add entity error");

				return next(responseObj);
			});
		})//end of findBusinessEntity Function
		
	},// End of addBusinessEntity function

//----------------------------------------------------------------------------------------------------------//


	updateBusinessEntity:function(reqBody,token,next){

		//default responseObj object
  		var responseObj = {
  			"statusCode": -1,
			"message": null,
			"result": null
  		};

	// For adding logs -------------------------------------------------------//

		//variable defined to form requestApi
        var requestApi  = ServConfigService.getApplicationConfig().base_url
                        + ":"
                        + ServConfigService.getApplicationAPIs().updateBusinessEntity.port
                        + ServConfigService.getApplicationAPIs().updateBusinessEntity.url


        //Variable defined to create dataLogObject
        var dataLogObject = {
            "userId":reqBody.frontendUserInfo.userId,
            "requestApi":requestApi,
            "eventType":"Update",
            "collection":"BusinessEntityStagingMaster"
        }
    //-----------------------------------------------------------------------//


		var businessInfo = reqBody.businessEntityDetails;
		var businessId = businessInfo.businessId;
		delete businessInfo.businessId;

		console.log('businessEntityDetails',businessInfo);

		BusinessEntityStagingMaster.update({businessId:businessId,isDeleted:false},businessInfo).exec(function (err,updatedBusinessEntity){
			console.log("Business Entity: updated record is",updatedBusinessEntity);
  			if (err) {
				responseObj.message = err ;
				sails.log.error("BusinessEntityStagingMaster-Model>updateBusinessEntity>BusinessEntityStagingMaster.update ","req body:"+ businessInfo," error:"+err);
				//next(responseObj);  					
  			} 
  			else if (!updatedBusinessEntity){      	// handling in case undefined/no object was returned
  				responseObj.message = "Undefined Object was returned" ;
  				sails.log.error("BusinessEntityStagingMaster-Model>updateBusinessEntity>BusinessEntityStagingMaster.update ","req body:"+ businessInfo," result:"+updatedBusinessEntity);
				//next(responseObj); 	
  			}
  			else if (updatedBusinessEntity.length === 0){      	// handling in case records was not updated
  				responseObj.statusCode = 2;
  				responseObj.message = "Business Entity updation failed::Business Entity not found!!!" ;
  				sails.log.error("BusinessEntityStagingMaster-Model>updateBusinessEntity>BusinessEntityStagingMaster.update ","req body:"+ businessInfo," result:"+updatedBusinessEntity);
				dataLogObject.newValue = updatedBusinessEntity;
                LogService.addDataLog(dataLogObject,token);		
				//next(responseObj); 	
  			}
  			else {   						// record successfully updated
  				responseObj.statusCode = 0;
  				responseObj.message = "Business Entity updated successfully!!";
  				responseObj.result = updatedBusinessEntity[0];
  				sails.log.info("BusinessEntityStagingMaster-Model>updateBusinessEntity>BusinessEntityStagingMaster.update : Client record updated successfully!!");
				//next(responseObj);
			  }	

			next(responseObj);		  	
		});
	
	},


//----------------------------------------------------------------------------------------------------------//


/*Function to delete Business Entity and its users*/
	deleteBusinessEntity:function(reqBody,token,next){

		//default response object
		var responseObj = {
            statusCode:-1,
            message:null,
            result:null	
        };

        console.log("deleting business entitty", reqBody);

    // For adding logs -------------------------------------------------------//

		//variable defined to form requestApi
        var requestApi  = ServConfigService.getApplicationConfig().base_url
                        + ":"
                        + ServConfigService.getApplicationAPIs().deleteBusinessEntity.port
                        + ServConfigService.getApplicationAPIs().deleteBusinessEntity.url


        //Variable defined to create dataLogObject
        var dataLogObject = {
            "userId":reqBody.frontendUserInfo.userId,
            "requestApi":requestApi,
            "eventType":"delete",
            "collection":"BusinessEntityStagingMaster"
        };
    //-----------------------------------------------------------------------//

        var businessId = reqBody.businessId;	//business Id to be deleted

        var usersToBeDeleted = [];				// users that are only mapped to this BE
        var usersDeleting = false;				// flag to check if operation is required

        var revertBE = false;					// flag to revert back deleted BE 
        var errTrack;	// to track which function sent error


        var rollbackSuccessStatus = true;		// status of rollback operations
        var rollbackFailureMessage = "";		// messages in case rollback failed


        async.series([		
			function(callback) {	//first function to delete BE
				console.log("In F1");
				var updateInfo = {			// info to be updated
        			isDeleted:true
        		};
			   	CommonOperations.updateRecord(BusinessEntityStagingMaster,"businessId",[businessId],updateInfo,token,function(response){
					if(response.statusCode === 0){
						console.log("F1:Success",response);
						dataLogObject.oldValue = response.result;
						LogService.addDataLog(dataLogObject,token);
						callback(null,response);
					}
					else{
						console.log("F1:Failure",response);
						errTrack = 0;
						callback("F1 failed",response);
					}
			   	});// end of update
			   				
			},
			function(callback) {	//second function to find all the users that are mapped to BE
				console.log("In F2");
				var response = {
					statusCode:-1,
					message:null
				}
			    BusinessEntityUserStagingMaster.native(function(err,Coll){	
					if(err){
						revertBE = true;
						errTrack = 1;
						sails.log.error("BusinessEntityStagingMaster>getBusinessEntity>BusinessEntityStagingMaster.find(asyncEach) req body:",reqBody," error:"+error);
						response.message = error;
						callback("F2 failed",response);
					}
					else{
						Coll.find({"businessId":{$elemMatch:{$eq:businessId}},"isDeleted":false}).toArray(function(error,userRecords){
							console.log("after native find",userRecords,"error",error);
							if(error){
								response.message = error;
								revertBE = true;
								sails.log.error("BusinessEntityStagingMaster>getBusinessEntity>BusinessEntityStagingMaster.find(asyncEach) req body:",reqBody," error:"+error);
								errTrack = 1;
								callback("F2 failed",response);
							}else if(userRecords === undefined){	// handling in case of undefined
								response.message = "Business entity user object is undefined";
								revertBE = true;
								errTrack = 1;
								callback("F2 failed",response);
							}else if(userRecords.length<=0){		// no users found
								response.statusCode=2;
								revertBE = true;
								response.message = "Unable to find users of this entity";
								errTrack = 1;
								callback("F2 failed",response);
							}else{
								response.statusCode=0;
								response.message = "User Records found successfully!!";
								console.log("F2:Success",response);
								usersDeleting = true;
								var userRecordsLength = userRecords.length;
								var i;
								for(i=0;i<userRecordsLength;i++){
									usersToBeDeleted.push(userRecords[i].userId);	
								}								
								callback(null,'success');
							}
						});	// end of find
					}	
				});	// end of native				
			},
			function(callback) {	//third function to delete users mapped to this BE only
				console.log("In F3");
				var updateInfo = {isDeleted:true};
			   	if (usersDeleting){
			   		CommonOperations.updateRecord(BusinessEntityUserStagingMaster,"userId",usersToBeDeleted,updateInfo,token,function(response){
						if(response.statusCode === 0){
							console.log("F3:Success",response);
							dataLogObject.collection = "BusinessEntityUserStagingMaster";
							dataLogObject.newValue = response.result;
							LogService.addDataLog(dataLogObject,token);
							callback(null,response);
						}
						else{
							revertBE = true;
							console.log("F3:Failure",response);
							response.message = "No Users Records found for this Business Entity";
							errTrack = 2;
							callback("F3 failed",response);
						}
			   		})// end of update
			   	}
				else{
					callback(null,"skip")	// skip to next function
				}		
			}
		],// end of array of functions
			function(error, result) {	// Final Function
				console.log("error is",error," result is",result);
				if(error){	// One of the three operations failed
					responseObj.statusCode = result[errTrack].statusCode;
					responseObj.message = result[errTrack].message;
					if(error ==="F1 failed"){	//rollback not required
						next(responseObj);
					}
					else{			// perform RollBack Operations					
						async.parallel([		
							function(callback1) {	//first function to rollback deleted BE
								if(revertBE){
									console.log("In R1");
									CommonOperations.updateRecord(BusinessEntityStagingMaster,"businessId",[businessId],{isDeleted:false},token,function(response){
										if(response.statusCode === 0){
											console.log("R1:Success",response);
										}
										else{
											console.log("R1:Failure",response);
											rollbackSuccessStatus = false;
			                        		rollbackFailureMessage = rollbackFailureMessage+"Rollback failed for reverting  Business Entity for id:"+businessId+".";											console.log("F2:Failure",response);
										}
										callback1(null,response);
							   		})// end of update
								}
								else{
									callback1(null,"skip1");	// skip to next function
								}			   											
							}
						],//end of parallel array of functions
						function(err,rollBackResults){
							console.log(rollBackResults);
					  		if(rollbackSuccessStatus===false){	// ROLLBACK failed
					  			responseObj.statusCode = -2;
					  			responseObj.message = rollbackFailureMessage;
					  		}

							next(responseObj);
						})// end of async parallel
					}
				}// end of error block
				else{	// success: BE and users deleted successfully
					responseObj.statusCode = 0;
					responseObj.message = "Business Enitity and its users successfully deleted";

					next(responseObj);
				}
		});//end of async series

	},
	


//----------------------------------------------------------------------------------------------------------//
	

	//Function to get existing business entities and their user
	getBusinessEntity:function(reqBody,token,next){

		//Variable declaration
		var searchBusinessEntity = reqBody.searchBusinessEntity;
		var businessId = reqBody.businessId;
		var skip = reqBody.skip;
		var limit = reqBody.limit;
		var state =  reqBody.state;
		var city = reqBody.city;
		var businessIdArray =  reqBody.businessIdArray;
		var kycVerified = reqBody.kycVerified;
		var doesUserAlreadyExist = reqBody.doesUserAlreadyExist;
		
		var businessEntityRecords = [];

		// Response object with default value.
		var responseObj = {
			statusCode:-1,
			message:null
		};
		var  queryString = {};
		
		//business array is used in user login
		if(businessId !== undefined){
			responseObj.result = {};
			queryString = {"isDeleted":false,"businessId":businessId,"limit":1};

		}
		else if(businessIdArray){	// to get records of multiple bsuiness IDS
			queryString = {"isDeleted":false,"businessId":businessIdArray};
			responseObj.result = [];
		}
		else{ 
		// this querystring object is used for search functionality.
			responseObj.result = [];
			queryString = {
				"isDeleted":false,
				"skip":skip,
				"limit":limit
			};
			if(doesUserAlreadyExist !== undefined){
				queryString.doesUserAlreadyExist = doesUserAlreadyExist;
			}

			if(kycVerified !== undefined){
 				queryString.kycVerified = kycVerified
 			}

			if(state !== undefined){
				queryString.state = state;
			}

			if(city !== undefined){
				queryString.city = city;
			}

			if(searchBusinessEntity !== undefined){
				queryString.or =[{businessName: new RegExp(searchBusinessEntity,"i")},{businessId:new RegExp(searchBusinessEntity,"i")}];
			}
		}

		console.log("queryString is",queryString);
		BusinessEntityStagingMaster.find(queryString).exec(function(error,businessEntityRecords){
				//console.log("staging records ",businessEntityRecords);
				if(error || businessEntityRecords === undefined){
					responseObj.message = error;
					sails.log.error("BusinessEntityStagingMaster>getBusinessEntity>BusinessEntityStagingMaster.find ","req body:",reqBody," error:"+error);

					return next(responseObj);
				}else{
					if (businessEntityRecords.length > 0){
						var count = 0;
						async.each(businessEntityRecords,function(record,callback){
							if(record !== undefined){
								BusinessEntityUserStagingMaster.native(function(err,Coll){
									if(err){
										responseObj.message = error;
										sails.log.error("BusinessEntityStagingMaster>getBusinessEntity>BusinessEntityStagingMaster.find(asyncEach) req body:",reqBody," error:"+error);
										callback();
									}
									else{
										Coll.find({"businessId":{$elemMatch:{$eq:record.businessId}},"isDeleted":false}).toArray(function(error,userRecords){
											if(error){
												responseObj.message = error;
												sails.log.error("BusinessEntityStagingMaster>getBusinessEntity>BusinessEntityStagingMaster.find(asyncEach) req body:",reqBody," error:"+error);
												callback();
											}else if(userRecords === undefined){
												responseObj.message = "Business entity user object is undefined";
												callback();
											}else if(userRecords.length<=0){
												if(count === 0){
													responseObj.statusCode=2;
													responseObj.message = "Unable to find users of this entity";
												}
												console.log("users for this BE not found");

												callback();
											}else{
												count++;
												var i;
							                    var recordsLength = userRecords.length;
							                    for(i=0;i<recordsLength;i++){
													//console.log("record is",userRecords[i])
							                    	if(userRecords[i].streetAddress){
								                        userRecords[i].streetAddress = EncryptDecryptService.decrypt(userRecords[i].streetAddress);
							                    	}
							                    	if(userRecords[i].email){
								                        userRecords[i].email = EncryptDecryptService.decrypt(userRecords[i].email);
							                    	}
							                    	if(userRecords[i].dateOfBirth){
								                        userRecords[i].dateOfBirth = EncryptDecryptService.decrypt(userRecords[i].dateOfBirth);							                
							                    	}
							                    	if(userRecords[i].password){
								                        userRecords[i].password = EncryptDecryptService.decrypt(userRecords[i].password);							                
							                    	}
							                    }
												record.businessEntityUsers = userRecords;
												//responseObj.businessEntityRecords.push(businessEntityRecords);
												if(businessId !== undefined){
													responseObj.result = record;
												}else{												
													responseObj.result.push(record);
												}
												responseObj.statusCode=0;	
												callback();
											}
										})
									}
								})
									
							}
						},function(error){
							if(error){
								responseObj.message = error;
								return next(responseObj);
							}else{
									if(responseObj.statusCode == 0){						
					                    if(skip === undefined || skip ===0){

					                 		delete queryString.limit; // to get count of sll records

					                    	// code to get total count of documents
					                   		CommonOperations.findCount("BusinessEntityStagingMaster",queryString,token,function(response){
						                        if(response.statusCode === 0){
						                            responseObj.statusCode = 0;
						                            responseObj.count = response.result;   // adding count to responseobj
													responseObj.message = "Records of business entity and their users are found successfully";
													sails.log.info("BusinessEntityStagingMasterModel>getBusinessEntity>BusinessEntityMaster.find: Records of business entity and their users are found successfully");
						                        }
						                        else{
						                        	responseObj.statusCode = response.statusCode;
						                            responseObj.message = response.message;
						                        }
						                        return next(responseObj);
						                    });// end of count code
					                    }
					                    else{
												responseObj.message = "Records of business entity and their users are found successfully";
												sails.log.info("BusinessEntityStagingMasterModel>getBusinessEntity>BusinessEntityMaster.find: Records of business entity and their users are found successfully");
												return next(responseObj)
										}
									}
									else{
										return next(responseObj)
									}
							}
						})	

				
					}
					else{
						responseObj.statusCode = 2;
						responseObj.message = "No Business Entity found";
						return next(responseObj);
					}
				}	
		})
	}//End of getBusinessEntity function 

};

