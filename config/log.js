/**
 * States.js
 *
 * @description     :: This module is used to create log file.
 * @createdBy       :: Nilesh
 * @created Date    :: 30-12-2016
 * @Last edited by  :: Nilesh
 * @last edited date::
 */


//Import winston module to create log file
var winston = require('winston');

module.exports = {

  /***************************************************************************
  *                                                                          *
  * Valid `level` configs: i.e. the minimum log level to capture with        *
  * sails.log.*()                                                            *
  *                                                                          *
  * The order of precedence for log levels from lowest to highest is:        *
  * silly, verbose, info, debug, warn, error                                 *
  *                                                                          *
  * You may also set the level to "silent" to suppress all logs.             *
  *                                                                          *
  ***************************************************************************/

    // level: 'info'
    'log': {
        'colors': false,
        'custom': new (winston.Logger)({
            'transports': [
                new (winston.transports.Console)({
                    'level': 'info',
                    'colorize': true,
                    'timestamp': false,
                    'json': false
                }),
                new winston.transports.File({
                    'name': 'info-file',
                    'level': 'info',
                    'colorize': false,
                    'timestamp': true,
                    'json': false,
                    'filename': 'BusinessEntityServiceInfo.log',
                    'maxsize': 5120000,
                    'maxFiles': 3,
                    'inspect': false
                }),
                new winston.transports.File({
                    'name': 'error-file', 
                    'level': 'error',
                    'colorize': false,
                    'timestamp': true,
                    'json': false,
                    'filename': 'BusinessEntityServiceErrors.log',
                    'maxsize': 5120000,
                    'maxFiles': 3,
                    'inspect': false
                })
               
               
            ]
        })
    }

};

