/**
 * BusinessEntityUsersMasterController
 *
 * @description :: Server-side logic for managing Businessentityusermasters
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	

	//Function to add business entity
	addBusinessEntityUser:function(req,res){
		console.log("Insied controller");

		var token = req.token;

		// user log details
		var userLogObject = req.body.frontendUserInfo;
		userLogObject.requestApi = req.baseUrl+ req.path;
		userLogObject.event = "Add Business Entity User";
		userLogObject.eventType = "Add";

		//Check for KYC done or not
		if(req.body.kycStatus === true){ // If KYC is done then add business entity to BusinessEntityMaster table(primary table)
			BusinessEntityUserMaster.addBusinessEntityUser(req.body.businessEntityUsers,token,function(response){
				res.json(response);
			});
		}else if(req.body.kycStatus === false){// If KYC is not done then add business entity to BusinessEntityStagingMaster table(secondary table)
			BusinessEntityUserStagingMaster.addBusinessEntityUser(req.body.businessEntityUsers,function(response){
				res.json(response);
			});
		}

	},



	//Function to add business entity
	updateBusinessEntityUser:function(req,res){
		console.log("Insied controller");

		var token = req.token;

		var userLogObject = req.body.frontendUserInfo;
		userLogObject.requestApi = req.baseUrl+ req.path;
		userLogObject.event = "Update Business Entity User";
		userLogObject.eventType = "Update";
		//Check for KYC done or not
		if(req.body.kycStatus === true){ // If KYC is done then add business entity to BusinessEntityMaster table(primary table)
			BusinessEntityUserMaster.updateBusinessEntityUser(req.body,token,function(response){
				res.json(response);
				// adding user log
				userLogObject.response = response.message;
		  		LogService.addUserLog(userLogObject,token);				
			});
		}else if(req.body.kycStatus === false){// If KYC is not done then add business entity to BusinessEntityStagingMaster table(secondary table)
			BusinessEntityUserStagingMaster.updateBusinessEntityUser(req.body,token,function(response){
				res.json(response);
				// adding user log
				userLogObject.response = response.message;
		  		LogService.addUserLog(userLogObject,token);
			});
		}

	},

	//Function to add business entity
	findBusinessEntityUser:function(req,res){
		console.log("Inside controller");

		var token = req.token;

		var userLogObject = req.body.frontendUserInfo;
		userLogObject.requestApi = req.baseUrl+ req.path;
		userLogObject.event = "Get Business Entity Users";
		userLogObject.eventType = "Get";
		//var token = req.token;
		//Check for KYC done or not
		if(req.body.kycStatus === true){ // If KYC is done then add business entity to BusinessEntityMaster table(primary table)
			BusinessEntityUserMaster.findBusinessEntityUser(req.body,function(response){
				res.json(response);
				// adding user log
				userLogObject.response = response.message;
		  		LogService.addUserLog(userLogObject,token);
			});
		}else if(req.body.kycStatus === false){// If KYC is not done then add business entity to BusinessEntityStagingMaster table(secondary table)
			BusinessEntityUserStagingMaster.findBusinessEntityUser(req.body,function(response){
				res.json(response);
				// adding user log
				userLogObject.response = response.message;
		  		LogService.addUserLog(userLogObject,token);
			});
		}

	},

		//Function to add business entity
	getBusinessEntityUsers:function(req,res){
		console.log("Inside controller");

		var token = req.token;

		var userLogObject = req.body.frontendUserInfo;
		userLogObject.requestApi = req.baseUrl+ req.path;
		userLogObject.event = "Get Business Entity Users";
		userLogObject.eventType = "Get";
		//var token = req.token;
		//Check for KYC done or not
		if(req.body.kycStatus === true){ // If KYC is done then add business entity to BusinessEntityMaster table(primary table)
			BusinessEntityUserMaster.getBusinessEntityUsers(req.body,token,function(response){
				res.json(response);
				// adding user log
				userLogObject.response = response.message;
		  		LogService.addUserLog(userLogObject,token);
			});
		}else if(req.body.kycStatus === false){// If KYC is not done then add business entity to BusinessEntityStagingMaster table(secondary table)
			BusinessEntityUserStagingMaster.getBusinessEntityUsers(req.body,token,function(response){
				res.json(response);
				// adding user log
				userLogObject.response = response.message;
		  		LogService.addUserLog(userLogObject,token);
			});
		}

	},



	//Function to add business entity
	loginBusinessEntity:function(req,res){
		console.log("Inside controller");

		//Check for KYC done or not
			BusinessEntityUserStagingMaster.loginBusinessEntity(req.body,function(response){
				res.json(response);
			});
		

	},



	
};

