/**
 * BusinessEntityMaster.js
 *
 * @description     :: To add, update and delete the business entities.
 * @createdBy       :: Nilesh
 * @created Date    :: 28-12-2016
 * @Last edited by  :: Ajeet
 * @last edited date::
 */


/**** Status code

    0  => success
    -1 => Error
    1  => Authentication failed
    2  => Data not found
    3  => Duplicate value exist.

****/

"use strict";

//Import promise module.
var promise = require('promise');
var async = require('async');
var request = require('request');
var MongoClient = require('mongodb').MongoClient;


// code to accept self-signed certifcate
if ('development' == sails.config.environment) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
};

module.exports = {

	//Table name and attributes of table
  	tableName:'BusinessEntityMaster',
	attributes:{
		businessId:{
			type:'string',
			primaryKey:true,
			unique:true,
			required:true
		},
		businessName:{
			type:'string',
			required:true
		},
		state:{
			type:'string',
			required:true
		},
		city:{
			type:'string',
			required:true
		},
		area:{
			type:'string'
		},
		landmark:{
			type:'string'
		},
		streetAddress:{
			type:'string',
			required:true
		},
		pincode:{
			type:'integer',
			required:true
		},
		yearOfEstablishment:{
			type:'integer'/*,
			required:true
*/		},
		dealsWithBrands:{
			type:'array'
		},
		registrationNumber:{
			type:'string'/*,
			required:true
*/		},
		starPointEarned:{
			type:'integer',
			defaultsTo:0,
		},
		monthlyTurnOver:{
			type:'string'
		},
		approxShopSize:{
			type:'string'
		},
		familyCode:{
			type:'string',
			defaultsTo:null
		},
		// fields added later
		weeklyOff:{
			type:'array'
		},
		registrationProof:{
			type:'string'
		},
		// ---end
		programsInterested:{
			type:'array'
		},
		programsMapped:{
			type:'array'
		},
		/*added later*/
		gvStatus:{
			type:'boolean',
			defaultsTo:'false'
		},
		kycVerified:{
			type:'boolean'
		},
		/*--------*/
		kycStatus:{
			type:'boolean',
			required:true,
			defaultsTo:true
		},
		createdBy:{
			type:'string',
			required:'true'
		},
		updatedBy:{
			type:'string'
		},
		dataSource:{
			type:'string',
			required:true
		},
		isStaging:{
			type:'boolean',
			defaultsTo:false
		},
		tag:{
			type:'string'
		},
		isDeleted:{
			type:'boolean',
			defaultsTo:false
		}


	},

// ***************************************************************************************************//

	//Function to add business entity
	addBusinessEntity:function(reqBody,token,next){

		////console.log("Inside add addBusinessEntity");

		//Variable declaration
		var businessEntityDetails = reqBody.businessEntityDetails; // To store business entity details
		var businessEntityUsers = reqBody.businessEntityUsers;// To store business entity user details
		var responseObj = {};// Response object is created.
		var businessId = null; // To store business entity id.

		// Response object with default value.
		var responseObj = {
			statusCode:-1,
			message:null
		};

		// KYC status will be true for main table and is Staging will be false
		businessEntityDetails.kycStatus = true;
		businessEntityDetails.isStaging = false;


	// ^^^^^^^^^^^^^^^^For adding logs ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^//

		//variable defined to form requestApi
        var requestApi  = ServConfigService.getApplicationConfig().base_url
                        + ":"
                        + ServConfigService.getApplicationAPIs().addBusinessEntity.port
                        + ServConfigService.getApplicationAPIs().addBusinessEntity.url


        //Variable defined to create dataLogObject
        var dataLogObject = {
            "userId":reqBody.frontendUserInfo.userId,
            "requestApi":requestApi,
            "eventType":"Add",
            "collection":"BusinessEntityMaster"
        }
    //-----------------------------------------------------------------------//

		//Generate business id if Business Enitity is added directly i.e not migrated
		if (businessEntityDetails.businessId === undefined){
			var timeStamp = new Date().getTime();
			businessId = "BE"+timeStamp;

			//To add KYC field and business id to business entity details object.
			businessEntityDetails.businessId = businessId;

			// to append business id to each user
			var i;
			for(i=0;i<businessEntityUsers.length;i++){
				businessEntityUsers[i].businessId = [];		//array of business ids field
				businessEntityUsers[i].businessId.push(businessId);	
			};
		}
		else if(businessEntityDetails.type === "BulkUpload"){
			businessId = businessEntityDetails.businessId;
			var i;
			for(i=0;i<businessEntityUsers.length;i++){
				businessEntityUsers[i].businessId = [];		//array of business ids field
				businessEntityUsers[i].businessId.push(businessEntityDetails.businessId);
				if(businessEntityUsers[i].password=== "" || businessEntityUsers[i].password=== null || businessEntityUsers[i].password=== undefined){
					responseObj.message = "Password field is required";
					return next(responseObj);
				}	
			}

		}
		else{	//storing business id in case of migration
			businessId = businessEntityDetails.businessId;
		}	
		

		// create empty programs mapped array
		if(businessEntityDetails.programsMapped === undefined){
			businessEntityDetails.programsMapped = [];
		}

		// Code for Encryption
		businessEntityDetails.streetAddress = EncryptDecryptService.encrypt(businessEntityDetails.streetAddress);

		//To add business entity
		var addBusinessEntityPromise = new promise(function(resolve,reject){

			BusinessEntityMaster.create(businessEntityDetails).exec(function(error,records){
				////console.log("records ",records);
				if(error || records === undefined){
					responseObj.message = error;
					sails.log.error("BusinessEntityMaster>addBusinessEntity>BusinessEntityMaster.create ","req body:"+ businessEntityDetails," error:"+error);
					reject();
				}else{
					records.streetAddress = EncryptDecryptService.decrypt(records.streetAddress);  // decryption
					responseObj.result = records;
					dataLogObject.newValue = records;
                    LogService.addDataLog(dataLogObject,token);			//adding data log
					sails.log.info("BusinessEntityMaster>addBusinessEntity>BusinessEntityMaster.find: Business entity has been added successfully");
					resolve();
				}
			})

		});

		// ON SUCCESS add business entity user master
		addBusinessEntityPromise.then(function(){
			////console.log("add entity success")
			// only BE has to be added
			console.log("length",businessEntityUsers.length,"familyCode",businessEntityDetails.familyCode);
			if(businessEntityUsers.length === 0 && businessEntityDetails.familyCode !== undefined){

				responseObj.statusCode = 0;
				responseObj.message = "Business Entity successfully added !!";
				return next(responseObj);
			}
			else{
				console.log("Inside else condition");
			 //To add business entity user call addBusinessEntityUser of BusinessEntityUsersMaster.
				BusinessEntityUserMaster.addBusinessEntityUser(businessEntityUsers,token,function(res){
					console.log("res of adding BE users",res);
				 	if(res.statusCode === 0){
				 		responseObj.message = "Business entity and user has been added successfully";
						responseObj.statusCode = 0;
						responseObj.result.businessEntityUsers = res.result;	     // append the user object

						sails.log.info("BusinessEntityMaster>addBusinessEntity>BusinessEntityMaster.find: Business entity and user has been added successfully");
				 		next(responseObj);
				 	}
				 /*	else if(res.statusCode === 3){
				 		responseObj.statusCode = 3;
				 		responseObj.message = "Record Creation Failed:"+ res.message;
				 		responseObj.result = res.result;
				 		next(responseObj);
				 	}*/
				 	else{
				 		//If user is not added then distroy the current bussiness entity.
				 		////console.log("businessId is",businessId);
				 		BusinessEntityMaster.destroy({businessId:businessId}).exec(function(error,deletedRecord){
							console.log("destroy:businessId",businessId,"error",error,"deletedRecord",deletedRecord);	
							if(error || !deletedRecord){
								////console.log("unable to destroy record");
								responseObj.statusCode = -2;
								responseObj.message = "Contact Admin : Business entity created but failed to create user";
								responseObj.result = businessId;
								sails.log.error("BusinessEntityMaster>addBusinessEntity>BusinessEntityMaster.destroy ","req body:"+ businessEntityDetails," error:"+error);
							}else{
								if(deletedRecord.length === 0){
									////console.log("unable to destroy record");
									responseObj.statusCode = -2;
									responseObj.message = "Contact Admin : Business entity created but failed to create user";
									responseObj.result = businessId;
									sails.log.error("BusinessEntityMaster>addBusinessEntity>BusinessEntityMaster.destroy ","req body:"+ businessEntityDetails," error:"+error);
								}
								else{
									////console.log("deletedRecord",deletedRecord);
									responseObj.statusCode = res.statusCode;
									responseObj.message = "Record not added:" + res.message;
									sails.log.info("BusinessEntityMaster>addBusinessEntity>BusinessEntityMaster.destroy: Record successfully deleted");
									responseObj.result = null;
									dataLogObject.eventType = "destroy";
									dataLogObject.oldValue = deletedRecord;
									LogService.addDataLog(dataLogObject,token);
								}					
							}
							 next(responseObj);
						});	// end of destroy
				 	}

				 	//return next(responseObj);
				});	// end of add user
			}
		},function(){
			//console.log("add entity error");
			return next(responseObj);
		});
		
	},// End of addBusinessEntity function





// ***************************************************************************************************//

	//function to simple update the BE
	updateBusinessEntity:function(reqBody,token,next){

		//default responseObj object
  		var responseObj = {
  			"statusCode": -1,
			"message": null,
			"result": null
  		};

  	// For adding logs -------------------------------------------------------//

		//variable defined to form requestApi
        var requestApi  = ServConfigService.getApplicationConfig().base_url
                        + ":" 
                        + ServConfigService.getApplicationAPIs().addBusinessEntity.port
                        + ServConfigService.getApplicationAPIs().addBusinessEntity.url

        //Variable defined to create dataLogObject
        var dataLogObject = {
            "userId":reqBody.frontendUserInfo.userId,
            "requestApi":requestApi,
            "eventType":"update",
            "collection":"BusinessEntityMaster"
        }
    //-----------------------------------------------------------------------//



		var businessInfo = reqBody.businessEntityDetails;	
		var businessId = businessInfo.businessId;
		delete businessInfo.businessId;

		// code for encryption
		if(businessInfo.streetAddress !==undefined){
			businessInfo.streetAddress = EncryptDecryptService.encrypt(businessInfo.streetAddress);
		}

		//console.log('businessEntityDetails',reqBody);

		BusinessEntityMaster.update({businessId:businessId,isDeleted:false},businessInfo).exec(function (err,updatedBusinessEntity){
			//console.log("Business Entity: updated record is",updatedBusinessEntity);
  			if (err) {
				responseObj.message = err ;
				sails.log.error("BusinessEntityMaster-Model>updateBusinessEntity>BusinessEntityMaster.update ","req body:"+ businessInfo," error:"+err);
  			} 
  			else if (!updatedBusinessEntity){      	// handling in case undefined/no object was returned
  				responseObj.message = "Undefined Object was returned" ;
  				sails.log.error("BusinessEntityMaster-Model>updateBusinessEntity>BusinessEntityMaster.update ","req body:"+ businessInfo," result:"+updatedBusinessEntity);
  			}
  			else if (updatedBusinessEntity.length === 0){      	// handling in case records was not updated
  				responseObj.statusCode = 2;
  				responseObj.message = "Business Entity updation failed::Business Entity not found!!" ;
  				sails.log.error("BusinessEntityMaster-Model>updateBusinessEntity>BusinessEntityMaster.update ","req body:"+ businessInfo," result:"+updatedBusinessEntity);
				dataLogObject.newValue = updatedBusinessEntity;
                LogService.addDataLog(dataLogObject,token);			//adding data log
  			}
  			else {   						// record successfully updated
  				responseObj.statusCode = 0;
  				responseObj.message = "Business Entity updated successfully!!";
  				updatedBusinessEntity[0].streetAddress = EncryptDecryptService.decrypt(updatedBusinessEntity[0].streetAddress);
  				responseObj.result = updatedBusinessEntity[0];
  				sails.log.info("BusinessEntityMaster-Model>updateBusinessEntity>BusinessEntityMaster.update : Client record updated successfully!!");
				//next(responseObj);
			  }	
			next(responseObj);
		});
	
	},


// ***************************************************************************************************//

	/*Function to delete Business Entity and its users*/
	deleteBusinessEntity:function(reqBody,token,next){

		//default response object
		var responseObj = {
            statusCode:-1,
            message:null,
            result:null	
        };


    // For adding logs -------------------------------------------------------//

		//variable defined to form requestApi
        var requestApi  = ServConfigService.getApplicationConfig().base_url
                        + ":"
                        + ServConfigService.getApplicationAPIs().deleteBusinessEntity.port
                        + ServConfigService.getApplicationAPIs().deleteBusinessEntity.url


        //Variable defined to create dataLogObject
        var dataLogObject = {
            "userId":reqBody.frontendUserInfo.userId,
            "requestApi":requestApi,
            "eventType":"delete",
            "collection":"BusinessEntityMaster"
        }
    //-----------------------------------------------------------------------//

        //console.log("deleting business entitty", reqBody);

        var businessId = reqBody.businessId;	//business Id to be deleted

        var usersToBeDeleted = [];				// users that are only mapped to this BE
        var usersToBeUnMapped = [];				// users mapped to multiple BE's
        var usersUnmapping = false;				// flag to check if operation is required
        var usersDeleting = false;				// flag to check if operation is required

        var revertBE = false;					// flag to revert back deleted BE 
        var revertDeletedBEUsers = false;		// flag to revert back deleted BE users 
        var revertUnMappedBEUsers = false;		// flag to revert unmapped BE users

     

        var rollbackSuccessStatus = true;		// status of rollback operations
        var rollbackFailureMessage = "";				// messages in case rollback failed

        var errTrack;	// to track which function sent error


        async.series([
        	// function to find Business Entity and check if programs are mapped		
			function(callback){
				var response = {statusCode:-1,message:null};
				BusinessEntityMaster.findOne({businessId:businessId,isDeleted:false}).exec(function(err,record){
					if(err){
						errTrack = 0;						
						responseObj.message = err;
						callback("F0 failed",response);
					}
					else if(!record){
						errTrack = 0;
						response.message = "BE record not found!!";
						response.statusCode = 2;
						callback("F0 failed",response);
					}
					else{
						if(record.programsMapped.length>0){
							errTrack = 0;
							response.statusCode = 3;
							response.message = "Business entity cannot be deleted as programs are mapped to it!!";
							callback("F0 failed",response);
						}
						else{
							callback(null,"success");
						}
					}
				})
			},
			function(callback) {	//first function to delete BE
				//console.log("In F1");
				var updateInfo = {			// info to be updated
        			isDeleted:true
        		};
			   	CommonOperations.updateRecord(BusinessEntityMaster,"businessId",[businessId],updateInfo,token,function(response){
					if(response.statusCode === 0){
						//console.log("F1:Success",response);
						dataLogObject.newValue = response.result;
						LogService.addDataLog(dataLogObject,token);
						callback(null,response);
					}
					else{
						//console.log("F1:Failure",response);
						errTrack = 1;
						callback("F1 failed",response);
					}
			   	});// end of update
			   				
			},
			function(callback) {	//second function to find all the users that are mapped to BE
				//console.log("In F2");
				var response = {
					statusCode:-1,
					message:null
				}
			    BusinessEntityUserMaster.native(function(err,Coll){	
					if(err){
						revertBE = true;
						sails.log.error("BusinessEntityStagingMaster>getBusinessEntity>BusinessEntityStagingMaster.find(asyncEach) req body:",reqBody," error:"+error);
						response.message = error;
						errTrack = 2;
						callback("F2 failed",response);
					}
					else{

						Coll.find({"businessId":{$elemMatch:{$eq:businessId}},"isDeleted":false}).toArray(function(error,userRecords){
							//console.log("after native find",userRecords,"error",error);
							if(error){
								response.message = error;
								errTrack = 2;
								revertBE = true;
								sails.log.error("BusinessEntityStagingMaster>getBusinessEntity>BusinessEntityStagingMaster.find(asyncEach) req body:",reqBody," error:"+error);
								callback("F2 failed",response);
							}else if(userRecords === undefined){	// handling in case of undefined
								response.message = "Business entity user object is undefined";
								revertBE = true;
								errTrack = 2;
								callback("F2 failed",response);
							}else if(userRecords.length<=0){		// no users found
								response.statusCode=2;
								revertBE = true;
								response.message = "Unable to find users of this entity";
								errTrack = 2;
								callback("F2 failed",response);
							}else{
								response.statusCode=0;
								response.message = "Unable to find users of this entity";
								//console.log("F2:Success",response);
								var i =0;
								var userRecordsLength = userRecords.length;
								//check if user is mapped to multiple BE's
								for(i=0;i<userRecordsLength;i++){
									if(userRecords[i].businessId.length > 1){
										usersUnmapping = true;
										usersToBeUnMapped.push(userRecords[i].userId);	
									}
									else{
										usersDeleting =  true;
										usersToBeDeleted.push(userRecords[i].userId);	
									}
								}
								callback(null,'success');
							}
						});	// end of find
					}	
				});	// end of native				
			},
			function(callback) {	//third function to delete users mapped to this BE only
				//console.log("In F3");
				var updateInfo = {isDeleted:true};
			   	if (usersDeleting){
			   		CommonOperations.updateRecord(BusinessEntityUserMaster,"userId",usersToBeDeleted,updateInfo,token,function(response){
						if(response.statusCode === 0){
							//console.log("F3:Success",response);
							dataLogObject.collection = "BusinessEntityUserMaster";
							dataLogObject.newValue = response.result;
						    LogService.addDataLog(dataLogObject,token);
							callback(null,response);
						}
						else{
							revertBE = true;
							//console.log("F3:Failure",response);
							errTrack = 3;
							callback("F3 failed",response);
						}
			   		})// end of update
			   	}
				else{
					callback(null,"skip")	// skip to next function
				}		
			},
			function(callback) {	//fourth function to ummapped this BE from users
				//console.log("In F4");
				var response = {statusCode:-1,message:null};
			    if (usersUnmapping){
			    	//console.log(usersToBeUnMapped);
			    	BusinessEntityUserMaster.native(function(err,collection){
			            if(err){
			            		revertBE = true;
			            		revertDeletedBEUsers = true;
			                	//console.log("F4:Failure:native failed");
			                	response.message = err;
			                	errTrack = 4;
			                    callback("F4 failed",response);
			            }else{
			                collection.update({userId:{$in:usersToBeUnMapped}},{$pull:{businessId:businessId}},{multi:true},function (err, results) {
			    				if (err || !results){
			    					//console.log("F4:Failure:update error");
			    					revertBE = true;
			            			revertDeletedBEUsers = true;
			    					response.message = err;
			    					errTrack = 4;
			    					callback("F4 failed",response);
			    				}
			    				else if (results.result.nModified === 0){
			    					//console.log("F4:Failure:update failed");
			    					revertBE = true;
			            			revertDeletedBEUsers = true;			    					
			    					response.message = "User id not found for reverting unmapped BE";
			    					errTrack = 4;
			    					callback("F4 failed",response);
			    				}
			    				else{
			    					response.statusCode = 0;
							    	response.message = "BE successfully unmapped from users";
			                        //console.log("F4:Success:unmapped BE from users!!!",results);
			    					callback(null,"success");
			    				}
			    			});//end of update
			            }
			        });//end of native
			   	}
				else{
					callback(null,"skip")	// skip to next function
				}								
			},
			function(callback) {	//fith function to delete users credentials
				//console.log("In F5");
			   	if (usersDeleting){
			   		var updateInfo = {isDeleted:true};
			   		CommonOperations.updateRecord(Credentials,"userId",usersToBeDeleted,updateInfo,token,function(response){
						if(response.statusCode === 0){
							//console.log("F5:Success",response);
							dataLogObject.collection = "Credentials";
							dataLogObject.newValue = response.result;
						    LogService.addDataLog(dataLogObject,token);
							callback(null,response);
						}
						else{
							revertBE = true;
			            	revertDeletedBEUsers = true;
			            	revertUnMappedBEUsers = true;
							//console.log("F5:Failure",response);
							errTrack = 5
							callback("F5 failed",response);
						}
			   		})// end of update


			   	}
				else{
					callback(null,"skip")	// skip to next function
				}	
							
			}
		],// end of array of functions
			function(error, result) {	// Final Function
				//console.log(result);
				if(error){	// One of the three operations failed
					responseObj.statusCode = result[errTrack].statusCode;
					responseObj.message = result[errTrack].message;
					if(error ==="F1 failed" || error ==="F0 failed"){	//rollback not required
						next(responseObj);
					}
					else{			// perform RollBack Operations					
						async.parallel([		
							function(callback1) {	//first function to rollback deleted BE
								if(revertBE){
									//console.log("In R1");
									CommonOperations.updateRecord(BusinessEntityMaster,"businessId",[businessId],{isDeleted:false},token,function(response){
										if(response.statusCode === 0){
											//console.log("R1:Success",response);
										}
										else{
											//console.log("R1:Failure",response);
											rollbackSuccessStatus = false;
			                        		rollbackFailureMessage = rollbackFailureMessage+"Rollback failed for reverting  Business Entity for id:"+businessId+".";											//console.log("F2:Failure",response);
										}
										callback1(null,response);
							   		})// end of update
								}
								else{
									callback1(null,"skip1");	// skip to next function
								}			   											
							},
							function(callback1) {	//second function to rollback deleted BE users
								if(revertDeletedBEUsers === true && usersDeleting === true){
									//console.log("in R2");
									CommonOperations.updateRecord(BusinessEntityUserMaster,"userId",usersToBeDeleted,{isDeleted:false},token,function(response){
										if(response.statusCode === 0){
											//console.log("R2:Success",response);
										}
										else{
											//console.log("R2:Failure",response);
											rollbackSuccessStatus = false;
			                        		rollbackFailureMessage = rollbackFailureMessage+"Rollback failed for reverting Business Entity Users for id:"+usersToBeDeleted+".";											//console.log("F2:Failure",response);
										}
										callback1(null,response);
							   		})// end of update
								}
								else{
									callback1(null,"skip2");			//skip to next function	
								}								
							
							},
							function(callback1) {	//third function to rollback unmapped BE users
								if(revertUnMappedBEUsers === true && usersUnmapping === true){
									//console.log("In R3",usersToBeUnMapped);
									var response = {statusCode:-1,message:null};
									BusinessEntityUserMaster.native(function(err,collection){
							            if(err){
							                	//console.log("R3:Failure:native failed");
							                	response.message = err;
							                	rollbackSuccessStatus = false;
			                        			rollbackFailureMessage = rollbackFailureMessage+"Rollback failed for reverting Unmapped Business Entity Users for id:"+usersToBeUnMapped+".";											
			                        			//console.log("R3:Failure",response);
							                    callback1(null,response);
							            }else{
							                collection.update({userId:{$in:usersToBeUnMapped}},{$push:{businessId:businessId}},{multi:true},function (err, results) {
							    				if (err || !results){
							    					//console.log("R3:Failure:update error");
							    					rollbackSuccessStatus = false;
			                        				rollbackFailureMessage = rollbackFailureMessage+"Rollback failed for reverting Business Entity Users for id:"+usersToBeUnMapped+".";											
			                        				response.message = err;
							    				}
							    				else if (results.result.nModified === 0){
							    					//console.log("R3:Failure:update failed");
							    					rollbackSuccessStatus = false;
			                        				rollbackFailureMessage = rollbackFailureMessage+"Rollback failed for reverting Business Entity Users for id:"+usersToBeUnMapped+".";											
			                        				response.message = "User id not found for unmapping BE";
							    				}
							    				else{
							    					response.statusCode = 0;
							    					response.message = "Users successfully reverted and BE mapped";
							                        //console.log("R3:Success:unmapped BE from users!!!",results);
							    				}
							    				callback1(null,response);
							    			});//end of update
							            }
							        });//end of native
								}
								else{
									callback1(null,"skip3");	//skip to next function	
								}							   				
							}
						],//end of parallel array of functions
						function(err,rollBackResults){
							//console.log(rollBackResults);
					  		if(rollbackSuccessStatus===false){	// ROLLBACK failed
					  			responseObj.statusCode = -2;
					  			responseObj.message = rollbackFailureMessage;
					  		}
							next(responseObj);
						})// end of async parallel
					}
				}// end of error block
				else{	// success: BE and users deleted successfully
					responseObj.statusCode = 0;
					responseObj.message = "Business Enitity and its users successfully deleted";
					next(responseObj);
				}
		});//end of async series

	},

// ***************************************************************************************************//


	//Function to get existing business entities and their user
	getBusinessEntity:function(reqBody,token,next){

		////console.log(sails.config.connections.phoenix2MongodbServer2);
		//console.log(BusinessEntityMaster.connection);
		//console.log("connection used",sails.config.models.connection);

		if(reqBody.changeConnection){
			sails.config.models.connection = 'phoenix2MongodbServer';
		}

		//console.log("connection changed ",sails.config.models.connection);


		//BusinessEntityMaster.connection = db;

		//Variable declaration
		var searchBusinessEntity = reqBody.searchBusinessEntity;
		var businessId = reqBody.businessId;
		var skip = reqBody.skip;
		var limit = reqBody.limit;
		var state =  reqBody.state;
		var city = reqBody.city;
		var businessIdArray =  reqBody.businessIdArray;
		var kycVerified = reqBody.kycVerified;
		var gvStatus = reqBody.gvStatus;
		var programId = reqBody.programId;
		
		var businessEntityRecords = [];

		// Response object with default value.
		var responseObj = {
			statusCode:-1,
			message:null
		}
		
		var  queryString = {};

		//To store business entity array in response object.

		//business array is used in user login
		if(businessId != undefined){
			queryString = {"isDeleted":false,"businessId":businessId,"limit":1};
			responseObj.result = {};
		}
		else if(businessIdArray){	// to get records of multiple bsuiness IDS
			queryString = {"isDeleted":false,"businessId":businessIdArray};
			responseObj.result = [];

		}
		else{
			responseObj.result = [];
			// this querystring object is used for search functionality.
			queryString = {
				"isDeleted":false,
				"skip":skip,
				"limit":limit
			}
			if(programId !== undefined){
				
				queryString = {
					"isDeleted":false,
					"skip":skip,
					"limit":limit,
					"programsMapped.programId":programId
				}
			}

 			if(kycVerified !== undefined){
 				queryString.kycVerified = kycVerified
 			}

 			if(gvStatus !== undefined){
 				queryString.gvStatus = gvStatus
 			}

			if(state !== undefined){
				queryString.state = state;
			}

			if(city !== undefined){
				queryString.city = city;
			}
			if(searchBusinessEntity !== undefined){
				queryString.or =[{businessName: new RegExp(searchBusinessEntity,"i")},{businessId:new RegExp(searchBusinessEntity,"i")}];
				//queryString.or =[{businessName: {contains:searchBusinessEntity}},{businessId:{contains:searchBusinessEntity}}];

			}

		}

		//console.log("queryString is",queryString);
		//Function to get business entity.
		BusinessEntityMaster.find(queryString).exec(function(error,businessEntityRecords){
				//console.log("queryString:",queryString,"recordsLength",businessEntityRecords.length);
				if(error || businessEntityRecords == undefined){
					responseObj.message = error;
					sails.log.error("BusinessEntityMaster>getBusinessEntity>BusinessEntityMaster.find ","req body:",reqBody," error:"+error);
					return next(responseObj);
				}else{
					if (businessEntityRecords.length > 0){
						var count = 0; // count of BE with users successfully found
						async.each(businessEntityRecords,function(record,callback){
							if(record !== undefined){
								record.streetAddress = EncryptDecryptService.decrypt(record.streetAddress); // decryption
								BusinessEntityUserMaster.native(function(err,Coll){	
									if(err){
										responseObj.message = error;
										sails.log.error("BusinessEntityStagingMaster>getBusinessEntity>BusinessEntityStagingMaster.find(asyncEach) req body:",reqBody," error:"+error);
										callback();
									}
									else{
										//console.log("Seraching for BE: ",record.businessId);
										Coll.find({"businessId":{$elemMatch:{$eq:record.businessId}},"isDeleted":false}).toArray(function(error,userRecords){
											//console.log("after native find",userRecords.length);
											if(error){
												responseObj.message = error;
												sails.log.error("BusinessEntityStagingMaster>getBusinessEntity>BusinessEntityStagingMaster.find(asyncEach) req body:",reqBody," error:"+error);
												callback();
											}else if(userRecords === undefined){
												responseObj.message = "Business entity user object is undefined";
												callback();
											}else if(userRecords.length<=0){
												if(count === 0){
													responseObj.statusCode=2;
													responseObj.message = "Unable to find users of this entity";													
												}
												console.log("users for this BE not found");
												callback();
											}else{
												count++;
												var i;
							                    var recordsLength = userRecords.length;
							                    for(i=0;i<recordsLength;i++){
							                        userRecords[i].streetAddress = EncryptDecryptService.decrypt(userRecords[i].streetAddress);
							                        userRecords[i].email = EncryptDecryptService.decrypt(userRecords[i].email);
							                    	if(userRecords[i].dateOfBirth){
							                        	userRecords[i].dateOfBirth = EncryptDecryptService.decrypt(userRecords[i].dateOfBirth);							                    
							                    	}
							                    
							                    }
												record.businessEntityUsers = userRecords;
												if(businessId !== undefined){     // only one record fetched
													responseObj.result = record;
												}else{												
													responseObj.result.push(record);
												}
												responseObj.statusCode=0;	
												callback();
											}
										});	// end of find
									}	
								});	// end of native

							}
						},function(error){
							if(error){
								responseObj.message = error;
								sails.log.error("BusinessEntityMaster>getBusinessEntity>BusinessEntityUserMaster.find(ayncEach)","req body:",reqBody," error:"+error);
								//return next(responseObj);
							}else{
								if(responseObj.statusCode === 0){

					                    if(skip === undefined || skip ===0){

					                    	delete queryString.limit; // to get count of sll records

					                    	// code to get total count of documents
					                   		CommonOperations.findCount("BusinessEntityMaster",queryString,token,function(response){
						                        if(response.statusCode === 0){
						                            responseObj.statusCode = 0;
						                            responseObj.count = response.result;   // adding count to responseobj
													responseObj.message = "Records of business entity and their users are found successfully";
													sails.log.info("BusinessEntityStagingMasterModel>getBusinessEntity>BusinessEntityMaster.find: Records of business entity and their users are found successfully");
						                        }
						                        else{
						                        	responseObj.statusCode = response.statusCode;
						                            responseObj.message = response.message;
						                        }
						                        //console.log("final result length:",responseObj.result.length);
						                        return next(responseObj);
						                    });// end of count code
					                    }
					                    else{
												responseObj.message = "Records of business entity and their users are found successfully";
												sails.log.info("BusinessEntityStagingMasterModel>getBusinessEntity>BusinessEntityMaster.find: Records of business entity and their users are found successfully");
												return next(responseObj)
										}
								}
								else{
									return next(responseObj);	
								}
							}
						})//
					}
					else{
						responseObj.statusCode = 2;
						responseObj.message = "No Business Entity found";					
						return next(responseObj);
					}	
				}
		})
	},//End of getBusinessEntity function 

// ***************************************************************************************************//


//Update all business entities having same mobile number with unique family code.
	updateFamilyCode:function(businessIdArray,familyCode,token,next){

			// default response object
	  		var responseObj ={
				statusCode:-1,
				message:null,
				result:null
			};

			BusinessEntityMaster.update(businessIdArray,{familyCode:familyCode}).exec(function(error,updatedRecords){
				//console.log("updatedRecords",updatedRecords);
				if(error || updatedRecords == undefined){
					responseObj.message = error;
					return next(responseObj);
				}else{
					if(updatedRecords.length==0){
						responseObj.statusCode = 2;
						responseObj.message = "Business Entity not found";
					}
					else{
						responseObj.statusCode = 0;
						responseObj.message = "Business Entity records successfully updated!!";
						responseObj.result = updatedRecords;
					}	
				}
				return next(responseObj);							
			})

	},//End of udateFamilyCode function



  	migrateBusinessEntity:function(reqBody,token,next){

  		// default response object
  		var responseObj ={
			statusCode:-1,
			message:null,
			result:null
		};
  		
		//console.log("migrating ");

  		var businessEntityDetails =  reqBody.businessEntityDetails;   // Business entity to be added to staging table
  		var businessEntityUsers = reqBody.businessEntityUsers;		// Users to be added 
  		var existingBusinessEntityUsers = reqBody.existingBusinessEntityUsers; // existing business entities to be updated

  		var migrateBE = reqBody.migrateBE;

  		var i;
  		var j;

  		// req object to add BE and its users
  		var businessEntityReqObj = {
  			frontendUserInfo:reqBody.frontendUserInfo,
  			businessEntityDetails:businessEntityDetails,
  			businessEntityUsers:businessEntityUsers
  		};

  		// family code
  		var familyCode;
  		if(businessEntityDetails!==undefined){
  			familyCode = businessEntityDetails.familyCode;
  		}
  		// to store users id of existing users to be updated and business id's for which family code is to be updated
  		var existingUsersArray = [];	
  		var businessIdsArray = [];
  		var existingBEUserslength ;
  		if(existingBusinessEntityUsers!==undefined){
  			existingBEUserslength = existingBusinessEntityUsers.length;
	  		for(i=0;i<existingBEUserslength;i++){
	  			existingUsersArray.push(existingBusinessEntityUsers[i].userId);
	  			for(j=0;j<existingBusinessEntityUsers[i].businessId.length;j++){
	  				businessIdsArray.push(existingBusinessEntityUsers[i].businessId[j]);
	  			}
	  		}
  		} 
  		
  		// req object to delete BE users from staging table
  		var stagingUserIdsArray = [];
  		var businessEntityUsers2 = [];  // contain only new users to be added
  		var businessEntityUsersLength;
  		var count;

  		if(businessEntityUsers!== undefined){
  			businessEntityUsersLength= businessEntityUsers.length;
  			for(i=0;i<businessEntityUsersLength;i++){
	  			stagingUserIdsArray.push(businessEntityUsers[i].userId);
	  			count =0;
	  			for(j=0;j<existingBEUserslength;j++){
	  				if(businessEntityUsers[i].mobileNumber === existingBusinessEntityUsers[j].mobileNumber){
	  					count++;
	  					break;
	  				}
	  			}
	  			if (count === 0){
	  				businessEntityUsers2.push(businessEntityUsers[i]);
	  			}
	  		}
	  	}

	  	businessEntityReqObj.businessEntityUsers = businessEntityUsers2;

  		// flags to track operations to be rollback
  		var deleteBusinessEntity = false;			
  		var deleteBusinessEntityUsers = false;
  		var deleteUsersCredentials = false;
  		var revertFamilyCode = false;
  		var revertExistingBusinessEntityUsers = false;
  		var insertBusinessEntityStaging = false;


	  	var addedBusinessEntityId;   // to store business id of added Business entity
  		var addedUsersArray = [];	// to store users id of added users

  		var prevBusinessEntityDetails;  // to store previous staging BE
  		var rollbackSuccessStatus = true;	// status of rollback operations
  		var rollbackFailureMessage;			// messages in case rollback failed

  		//console.log("businessEntityReqObj",businessEntityReqObj);
  		//console.log("existingBusinessEntityUsers",existingBusinessEntityUsers);
  		//console.log("orignal businessEntityUsers",businessEntityUsers);  		
  		//console.log("stagingUserIdsArray",stagingUserIdsArray);


  		var message;
  		var smsObject;


  		/*AsyncWaterfall function :Runs a list of async tasks, passing the results of each into the next one*/
        async.waterfall([
            //first function : ADD BE and its USERS to MAIN table
            function(callback){
                if(businessEntityDetails !== undefined && businessEntityUsers !== undefined){
                    //console.log("in first function");
                    BusinessEntityMaster.addBusinessEntity(businessEntityReqObj,token,function (response){
                        if(response.statusCode === 0){
                        	//console.log("f1:business entity and users added");
                        	addedBusinessEntityId = response.result.businessId;  // id of BE (will be same)
                        	if(response.result.businessEntityUsers !== undefined){
                        		var usersLength = response.result.businessEntityUsers.length; //users id of added BE's
                        		for(i=0;i<usersLength;i++){
                        			addedUsersArray.push(response.result.businessEntityUsers[i].userId);
                        		}
                        	}

                            callback(null,'success');
                        }else{
                        	//console.log("f1:addition failed");
                            return callback("F1 failed",response);
                        }
                    })
                }else{
                    callback(null,'skip'); // skip go to second function
                }
            },
            //second function : UPDATE EXISTING USERS: (add business id field to the array)
            function(status,callback){
                if(existingBusinessEntityUsers !== undefined){ 
                	//console.log("existingUsersArray",existingUsersArray);
                    BusinessEntityUserMaster.native(function(err,collection){
                        if(err){
                        	//console.log("F2:native failed");
                        	deleteBusinessEntity = true;
                        	deleteBusinessEntityUsers = true;
                        	deleteUsersCredentials = true;
                        	return callback("UpdateExistingBusinessIdError","No result");//return function if validation failed
                        }else{
                        	collection.update({userId:{$in:existingUsersArray}},{$push:{businessId:addedBusinessEntityId}},{multi:true},function (err, results) {
    							if (err || !results){
    								//console.log("F2:update error");
                        			deleteBusinessEntity = true;
                        			deleteBusinessEntityUsers = true;
                        			deleteUsersCredentials = true;    								
                        			return callback("UpdateExistingBusinessIdError",{statusCode:-1,message:"Existing users update failed"});//return to Final Result function if validation failed
    							}
    							else if (results.result.nModified === 0){
    								//console.log("F2:update failed");
    								deleteBusinessEntity = true;
                        			deleteBusinessEntityUsers = true;
                        			deleteUsersCredentials = true;  
    								return callback("UpdateExistingBusinessIdError",{statusCode:-1,message:"Existing users update failed"});//return to Final Result function if validation failed
    							}
    							else{
    								//console.log("F2:users update SUCCESS",results);
    								callback(null,'success');	// go to next function
    							}
    						});//end of update
                        }
                    })//end of native
                }else{
                    callback(null,'skip');// skip  to third function
                }
            },
            //third function : UPDATE EXISTING BE's with FAMILY CODE
            function(status,callback){
                if(existingBusinessEntityUsers !== undefined){
                    //console.log("in third function");                    
                    BusinessEntityMaster.updateFamilyCode(businessIdsArray,familyCode,token,function(response){
						if(response.statusCode === 0){
                        	//console.log("f3:business entity updated");
                            callback(null,"success");

                        }else{
                        	//console.log("f3:addition failed");
    						deleteBusinessEntity = true;
                        	deleteBusinessEntityUsers = true;
                        	deleteUsersCredentials = true;
                        	revertExistingBusinessEntityUsers = true;
                            return callback("Business Entity familyCode updation failed",response);
                        }                    	
                    }); // end of update family code
                        	
  
                }
                else{
                    callback(null,'skip'); // skip  to fourth function
                }
            },
            //fourth function : DELETE BUSINESS ENTITY from STAGING Table
            function(status,callback){
                if(businessEntityDetails !== undefined && migrateBE === true){
                	//var destroyBusinessId=[businessId];
                    //console.log("in fourth function");

                	var reqFindBEObject={
                		businessId:addedBusinessEntityId
                	};
                	BusinessEntityStagingMaster.findOne(reqFindBEObject).exec(function(error,businessEntityRecord){
                		if(error || businessEntityRecord === undefined){
                			return callback("Business Entity Staging deletion failed",{statusCode:2,message:"businessEntity not found",result:null});
                		}
                		else{
                			prevBusinessEntityDetails = businessEntityRecord;
		                	CommonOperations.deleteRecord(BusinessEntityStagingMaster,"businessId",[addedBusinessEntityId],token,function(response){
								if(response.statusCode === 0){
		                        	//console.log("f4:Success:business entity record deleted staging");
		                            callback(null,'success');
		                        }else{
		                        	//console.log("f4:error: deletion failed");
		    						deleteBusinessEntity = true;
		                        	deleteBusinessEntityUsers = true;
		                        	deleteUsersCredentials = true;
		                        	revertExistingBusinessEntityUsers = true;
		                        	revertFamilyCode = true;
		                        	return callback("Business Entity Staging deletion failed",response);
		                        }                    	
		                    }); // end of update family code
                		}
                	})  
                        
                }
                else{
                    callback(null,'skip'); // skip  to fourth function
                }
            },
            //fifth function : DELETE BUSINESS ENTITY USERS from STAGING Table
            function(status,callback){
                if(businessEntityDetails !== undefined && migrateBE === true){
                    //console.log("in fifth function");

                    CommonOperations.deleteRecord(BusinessEntityUserStagingMaster,"userId",stagingUserIdsArray,token,function(response){
						if(response.statusCode === 0){
                        	//console.log("f5:Success:business entity record deleted staging");
                            callback(null,'success');
                        }else{
                        	//console.log("f5:error: addition failed");
    						deleteBusinessEntity = true;
                        	deleteBusinessEntityUsers = true;
                        	deleteUsersCredentials = true;
                        	revertExistingBusinessEntityUsers = true;
                        	revertFamilyCode = true;
                        	insertBusinessEntityStaging = true;
                        	return callback("Business Id addition failed",response);
                        }                    	
                    }); // end of destroy staging BE users      
                }
                else{
                    callback(null,'skip'); // skip  to fourth function
                }
            }//end of fifth function
        ],
        function(err,result){	//final function
            if(err){		// one of the functions failed, so perform ROLLBACK (except for first function);
                //console.log("In error",err,"In result",result);
                responseObj.statusCode = result.statusCode;
                responseObj.message = result.message;
                if(err==="F1 failed"){			// First Function failed, no rollback required
                		next(responseObj);
                }
                else{
	                // ASYNC PARALLEL To perform ROLLBACK operations simulataneously
	                //console.log("Performing Rollback;")
	                async.parallel([		
						function(callback1) {	//first function to delete added BE
							if(deleteBusinessEntity){
								//console.log("In R1");
			                    CommonOperations.deleteRecord(BusinessEntityMaster,"businessId",[addedBusinessEntityId],token,function(response){
									if(response.statusCode === 0){
			                        	//console.log("R1:Success:business entity record deleted!!!");
			                        }else{
			                        	//console.log("R1:Failure: deletion failed");
			                        	rollbackSuccessStatus = false;
			                        	rollbackFailureMessage = "Rollback failed for deleting Main Business Entity for id:"+addedBusinessEntityId+".";
			                        }
			                        callback1(null,1);         
			                    }); // end of destroy Main BE 
							}
							else{
								callback1(null,1);
							}
						},
						function(callback1) {	//second function to delete added BE Users
							if(deleteBusinessEntityUsers){
								//console.log("In R2");
								CommonOperations.deleteRecord(BusinessEntityUserMaster,"userId",addedUsersArray,token,function(response){
									if(response.statusCode === 0){
			                        	//console.log("R2:Success:business entity record deleted!!!");
			                        }else{
			                        	//console.log("R2:Failure: deletion failed");
			                        	rollbackSuccessStatus = false;
			                        	rollbackFailureMessage = rollbackFailureMessage+"Rollback failed for deleting Main Business Entity Users for ids:"+addedUsersArray+".";
			                        }
			                        callback1(null,2);         
			                    }); // end of destroy Main BE users 
							}
							else{
								callback1(null,2);
							}
						},
						function(callback1) {	//third function to delete added Users Credentials
							if(deleteUsersCredentials){
								//console.log("In R3");
								CommonOperations.deleteRecord(Credentials,"userId",addedUsersArray,token,function(response){
									if(response.statusCode === 0){
			                        	//console.log("R3:Success:business entity record deleted!!!");
			                        }else{
			                        	//console.log("R3:Failure: deletion failed");
			                        	rollbackSuccessStatus = false;
			                        	rollbackFailureMessage = rollbackFailureMessage+"Rollback failed for deleting  Business Entity Users Credentials for ids:"+addedUsersArray+".";
			                        }
			                        callback1(null,3);         
			                    }); // end of destroy BE users credentials
							}
							else{
								callback1(null,3);
							}
						},
						function(callback1) {	//fourth function to revert updated BE users
							if(revertExistingBusinessEntityUsers && addedUsersArray.length>0){
								//console.log("In R4");
								BusinessEntityUserMaster.native(function(err,collection){
			                        if(err){
			                        	//console.log("R4:Failure:native failed");
			                        	rollbackSuccessStatus = false;
			                        	rollbackFailureMessage = rollbackFailureMessage+"Rollback failed for reverting  Business Entity Users for ids:"+existingUsersArray+".";			           
			                        	callback1(null,4)
			                        }else{
			                        	collection.update({userId:{$in:existingUsersArray}},{$pop:{businessId:1}},{multi:true},function (err, results) {
			    							if (err || !results){
			    								//console.log("R4:Failure:update error");
			    								rollbackSuccessStatus = false;
			                        			rollbackFailureMessage = rollbackFailureMessage+"Rollback failed for reverting  Business Entity Users for ids:"+existingUsersArray+".";			           
			    							}
			    							else if (results.result.nModified === 0){
			    								//console.log("R4:Failure:update failed");
			    								rollbackSuccessStatus = false;
			                        			rollbackFailureMessage = rollbackFailureMessage+"Rollback failed for reverting  Business Entity Users for ids:"+existingUsersArray+".";			           			                        	
			    							}
			    							else{
			                        			//console.log("R4:Success:Reverted existing users!!!",results);
			    							}
			    							callback1(null,4)
			    						});//end of update
			                        }
			                    });//end of native
							}
							else{
								callback1(null,4);
							}
						},
						function(callback1) {	//fifth function to revert updated BE
							if(revertFamilyCode){
								//console.log("In R5");
								var famCode = null;
								BusinessEntityMaster.updateFamilyCode(businessIdsArray,famCode,token,function(response){
									if(response.statusCode === 0){
			                        	//console.log("R4:Succes:reverted successfully");
			                        }else{
			                        	//console.log("R4:Failure:revert failed");
			    						rollbackSuccessStatus = false;
			                        	rollbackFailureMessage = rollbackFailureMessage+"Rollback failed for reverting  Business Entity Users for ids:"+existingUsersArray+".";			           
			                        }
			                        callback1(null,5);
			                    }); // end of update family code
							}
							else{
								callback1(null,5);
							}

						},
						function(callback1) {	//sixth function to revert deleted BE staging table
							if(insertBusinessEntityStaging){
								//console.log("In R6");
								BusinessEntityStagingMaster.create(prevBusinessEntityDetails).exec(function(error,records){
									if(error || records === undefined){
										//console.log("R6:Failure:revert BE staging failed");
										sails.log.error("BusinessEntityStagingMasterModel>addBusinessEntity>BusinessEntityStagingMaster.create ","req body:"+ businessEntityDetails," error:"+error);
										rollbackSuccessStatus = false;
			                        	rollbackFailureMessage = rollbackFailureMessage+"Rollback failed for reverting  Business Entity Staging for id:"+existingUsersArray+".";			          
									}else{
										//console.log("R6:Failure:revert BE staging failed");
									}
									callback1(null,6)
								});	
							}
							else{
								callback1(null,6);
							}
						}
					],
					function(error, rollBackResults) {		// Final Function
					  //console.log(rollBackResults);
					  if(rollbackSuccessStatus===false){	// ROLLBACK failed
					  		responseObj.statusCode = -2;
					  		responseObj.message = rollbackFailureMessage;
					  		next(responseObj);
					  }
					  else{									// Rollback Success
					  		next(responseObj);
					  }
					});
				}	
            }//end of Waterfall Error
            else{	// error is null, so operations performed successfully
            	//console.log("Final F: success");
            	responseObj.statusCode = 0;
            	responseObj.message = "Business Entity and users successfully added to main table !!!";

            	for(i=0;i<businessEntityUsers.length;i++){
                	message =  "Your KYC Verification is successfully completed !!!";
                    smsObject = {mobileNumber:businessEntityUsers[i].mobileNumber,message:message};
                    CommunicationService.sendSms(smsObject,token);
                }

            	next(responseObj);
            }
        }) // end of async waterfall

  	},
//----------------------------------------------------------------------------------------------------------//


//----------------------------------------------------------------------------------------------------------//

    /*
    # Method: updateUserProgramsMapped
    # Description: to add a new program once the user is registered for it
    # Input: Model name,array of IDs to delete and callback function
    # Output : response with success or error
    */    

    updateProgramsMapped:function(reqBody,token,next){
    	


        //default responseObj object
        var responseObj = {
            "statusCode": -1,
            "message": null,
            "result": null,
            "userDetails":[]
        };

        var requestObject = reqBody; //requestObject to fetch businessEntity
        requestObject.businessIdArray = requestObject.records;
        delete requestObject.records;

        var emailIds = [];	//array of usersEmailId
        var userIds = [];	//array of userIds
        var mobileNumberArray = [];		//array of usersMobileNumber

        async.series([
        	function(callback){
        		BusinessEntityMaster.getBusinessEntity(requestObject,token,function(response){
        			if(response.statusCode === 0){

        				//console.log("response",response);
        				var businessEntityArray = response.result;
        				//iterate over businessEntity to fetch businessEntityDetails
        				for(var i = 0; i < businessEntityArray.length; i++) {
        					var businessEntityUserArray = businessEntityArray[i].businessEntityUsers;
        					var businessEntityObject = {};
        					var businessEntityUserObject ={};
        					var businessEntityUsers = [];
        					//user details object 
        					businessEntityObject.userId = businessEntityArray[i].businessId;
        					businessEntityObject.userName = businessEntityArray[i].businessName;
        					businessEntityObject.registrationNumber = businessEntityArray[i].registrationNumber;
        					businessEntityObject.state = businessEntityArray[i].state;
        					businessEntityObject.city = businessEntityArray[i].city;
        					businessEntityObject.streetAddress = businessEntityArray[i].streetAddress;
        					businessEntityObject.pincode= businessEntityArray[i].pincode;

        					//iterate over businessEntityUsersArray to fetch userDetails 
        					for(var j=0; j<businessEntityUserArray.length; j++){
        						
                            	businessEntityUserObject.firstName = businessEntityUserArray[j].firstName;
                            	businessEntityUserObject.lastName = businessEntityUserArray[j].lastName;
                            	businessEntityUserObject.userType = businessEntityUserArray[j].userType;
                            	businessEntityUserObject.mobileNumber = businessEntityUserArray[j].mobileNumber;
                            	if(businessEntityUserArray[j].userType === 'owner'){
									businessEntityObject.mobileNumber = businessEntityUserArray[j].mobileNumber;                            		
                            	}

                            	//Ids used to send email/sms
                            	emailIds.push(businessEntityUserArray[j].email);
                            	userIds.push(businessEntityUserArray[j].userId);
                            	mobileNumberArray.push(businessEntityUserArray[j].mobileNumber);

                            	console.log("userIds",userIds,emailIds,mobileNumberArray);
                            	
                            	businessEntityUsers.push(businessEntityUserObject);
                            } 
                            businessEntityObject.businessEntityUsers = businessEntityUsers;
                            responseObj.userDetails.push(businessEntityObject);
                        }
                        callback(null,'success');
        			}
        			else{
        				responseObj.message = response.message
        				callback("Error","Error occurred while fetching businessEntity");
        			}
        		})
        	}
        ],

        function(err, results) {
            if(err){
                    sails.log.info("Error while searching ClientUsers");
                    responseObj.message = err,
                    next(responseObj);    
                
            } else {
                ////console.log("After Getting Details ----------------",responseObj);
                var BusinessToBeMapped = requestObject.businessIdArray;  // records to be mappped
		        var programObj = requestObject.programObj;	// program to be mapped/unmapped

		        var type = requestObject.type;		// to identify whether to map or unmap
		        var queryString;
		        var successMessage;
		        if(type === "Add"){
		        	queryString = {$addToSet:{programsMapped:programObj}};
		            successMessage = "Business Entity successfully registered for program !!!";
		        }
		        else if (type === "Delete"){
		        	queryString = {$pull:{programsMapped:programObj}};
		            successMessage = "Business Entity successfully deregistered for program !!!";
		        }

		        //console.log("Ids",BusinessToBeMapped);
		        //console.log("querystring",queryString);
		        BusinessEntityMaster.native(function(err,collection){
		            if(err){
		                ////console.log("R3:Failure:native failed");
		                responseObj.message = err;
		            }else{
		                collection.update({businessId:{$in:BusinessToBeMapped}},queryString,{multi:true},function (err, results) {
		                    ////console.log(err,results);
		                    if (err || !results){
		                         ////console.log("R3:Failure:update error");
		                		responseObj.message = err;
		                    }
		                    else if (results.result.nModified === 0){
		                    	responseObj.statusCode = 2;
		                    	responseObj.message = "No Business Entity found or Program already registered!!";                    	
		                    }
		                    else{
		                        responseObj.statusCode = 0;
		                        responseObj.message = successMessage;
		                        console.log("responseObj",responseObj);

		                        console.log("userIds",userIds);
		                        for(var i=0; i<userIds.length; i++){
		                        	console.log("inside loop");
		                        	//REQUESToBJECT TO SEND SMS
                                    var smsInfo = {
                                        userId:userIds[i],
                                        mobileNumber:mobileNumberArray[i],
                                        message:ServConfigService.getApplicationConfig().transactionalMessages.businessEntityEnrolled
                                    }       

                                    //REQUESTOBJECT TO SEND EMAIL
                                    var emailInfo = {
                                        emailIds: emailIds,
                                        userIds: userIds,
                                        subject: ServConfigService.getApplicationConfig().transactionalMessages.businessEntityEnrolled,
                                        text: ServConfigService.getApplicationConfig().transactionalMessages.businessEntityEnrolled 
                                    } 
                                    console.log("smsInfo",smsInfo);
                                    console.log("emailInfo",emailInfo);
                                    //service call to send sms/email after refistration process is completed
                                    CommunicationService.sendSms(smsInfo,token);
		                        }
		                       		//service call to send email
                                CommunicationService.sendEmail(emailInfo,token);
		                    }
		                    ////console.log("Final Response is --------------------", responseObj);
		                    next(responseObj);
		                });//end of update
		            }
        		});//end of native          
            }
        }) // //console.log("After For") ;           
    }   
};
