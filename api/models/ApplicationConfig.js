/**
 * ApplicationConfig.js
 *
 * @created          :: Swati
 * @Created  Date    :: 4/01/2016
 */

"use strict";

module.exports = {
  //connection:'phoenix2MongodbServer',
  tableName: 'ApplicationConfig',
  attributes: {
  },
  
  /**
   *	Method		:: getApplicationConfigDetails
   *	Description :: used to retrieve Application Configurations
   *
   */
  getApplicationConfigDetails: function(setAppConfigCallback, passOnCallbak) {
    var appConfig;
    
    // Retrieve Application Configuration
    ApplicationConfig.find().limit(1)
      .exec(function (err, result) {
        if(result.length){
          
          appConfig = result[0];
          setAppConfigCallback(appConfig, passOnCallbak);
        }else if(!err){
          
          appConfig = false;
          sails.log.error("ApplicationConfig model > getApplicationConfigDetails > ApplicationConfig.find > ", result);
          setAppConfigCallback(appConfig, passOnCallbak);
          
        }else {
          
          appConfig = false;
          sails.log.error("ApplicationConfig model > getApplicationConfigDetails > ApplicationConfig.find > ", err);
          setAppConfigCallback(appConfig, passOnCallbak);
          
        }
      });
  }
};