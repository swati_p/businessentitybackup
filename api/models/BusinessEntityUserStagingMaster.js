/**
 * BusinessEntityUserStagingMaster.js
 *
 * @description     :: To add, update and delete the business entities user.
 * @createdBy       :: Nilesh
 * @created Date    :: 30-12-2016
 * @Last edited by  :: Nilesh
 * @last edited date::
 */


/**** Status code

    0  => success
    -1 => Error
    1  => Authentication failed
    2  => Data not found
    3  => Duplicate value exist.

****/

"use strict";
var promise = require('promise');

module.exports = {

	//Table name and attributes of table
  	tableName:'BusinessEntityUserStagingMaster',
  	attributes: {
  		businessId:{
  			type:'array',
			required:true
  		},
  		fullName:{
  			type:'string',
  			required:true
  		},
  		firstName:{
  			type:'string',
  			required:true
  		},
  		lastName:{
  			type:'string',
  			required:true
  		},
  		userId:{
  			type:'string',
  			unique:true,
  			required:true
  		},
  		mobileNumber:{
  			type:'string',
  			required:true,
  		},
  		email:{
  			type:'string'
//  		required:true,
  		},
  		dateOfBirth:{
  			type:'string'
//  		required:true
  		},
  		streetAddress:{
  			type:'string'
//  		required:true
  		},
  		password:{
  			type:'string',
            required:true
        },
  		gender:{
  			type:'string'
//  			required:true
  		},
  		state:{
  			type:'string'
//  		required:true
  		},
  		city:{
  			type:'string'
//  		required:true
  		},
  		pincode:{
  			type:'integer'
//  		required:true
  		},
        landmark:{
            type:'string'
        },
        area:{
            type:'string'
        },
  		userType:{
  			type:'string',
  			required:true
  		},
  		marriageAnniversary:{
  			type:'string',
  		},
  		maritalStatus:{
  			type:'string'
  		},
        /* fields added later*/
        noOfChildrens:{
            type:'integer'
        },
        ageOfChildren:{
            type:'string'
        },
        preferredLanguage:{
            type:'string'
        },
        idProof:{
            type:'string'
        },
        /* end */
  		createdBy:{
			 type:'string'
		},
		updatedBy:{
			 type:'string'
		},
  		isDeleted:{
  			type:'boolean',
  			defaultsTo:false
  		},
        alternateMobileNumber:{
            type:'array'
        }
  	},


/*    //Function to add business entity user (single user) and its credentials
    addBusinessEntityUser:function(businessEntityUsers,token,next){

        //Variable declaration
        var userId = null;
        var timeStamp = null;
        var queryString = [];
        var result;     // to store result
        var loginObj;
        var requestApi;
        var requestOptions;


        // Response object with default value.
        var responseObj = {
            statusCode:-1,
            message:null,
            result:null
        };



        //console.log("Business entity users:",businessEntityUsers);
         
        var userCredentials = [];  // to create array of credentials object (Credentials Service)
        var tempObject;            
        var userIdArray = [];   // array of user ids
        var mobileNumberArray = [];   // array of mobile numbers to find

        var businessEntityUsersLength = businessEntityUsers.length;

        for(i=0;i<businessEntityUsersLength;i++){
            // for user id
                timeStamp = new Date().getTime();
                userId = "BU"+timeStamp+i;
                businessEntityUsers[i].userId = userId;   
            
            userIdArray.push(userId);
            tempObject = {
                userId: userId,
                userName: businessEntityUsers[i].mobileNumber,
                password: businessEntityUsers[i].password,
                userType: "BusinessEntityUser"
            };

            userCredentials.push(tempObject);
            mobileNumberArray.push(businessEntityUsers[i].mobileNumber);    

            delete businessEntityUsers[i].password;   // password is not stored over here

            //Code for encryption
            if(businessEntityUsers[i].streetAddress){
                businessEntityUsers[i].streetAddress = EncryptDecryptService.encrypt(businessEntityUsers[i].streetAddress);
            }
            if(businessEntityUsers[i].email){
                businessEntityUsers[i].email = EncryptDecryptService.encrypt(businessEntityUsers[i].email);                
            }
            if(businessEntityUsers[i].dateOfBirth){
                businessEntityUsers[i].dateOfBirth = EncryptDecryptService.encrypt(businessEntityUsers[i].dateOfBirth);                
            }
       
        };


        //variable defined to form requestApi
        requestApi  = ServConfigService.getApplicationConfig().base_url
                        + ":"
                        + ServConfigService.getApplicationAPIs().addUserCredentials.port
                        + ServConfigService.getApplicationAPIs().addUserCredentials.url;

        //console.log("requestApi is",requestApi,"and userCredentials",userCredentials);

        requestOptions = {
                    url: requestApi, //ServConfigService.getUserPersonalDetailsAPI.url+"?userId="+foundCredentials.userId,
                    method: ServConfigService.getApplicationAPIs().addUserCredentials.method,
                    headers: {
                        'authorization' : 'Bearer ' + token
                    },
                    json:userCredentials
        };
        
        //console.log("requestOptions is",requestOptions);


        //console.log("mobileNumberArray",mobileNumberArray);
        // find object for findBusinessEntityUser
        var findObject = {
            kycStatus:true,
            mobileNumberArray:mobileNumberArray
        };

            BusinessEntityUserMaster.findBusinessEntityUser(findObject,function(response){
                if(response.statusCode === 0){
                    //console.log("mobile number found",response);
                    responseObj.statusCode = 3;
                    responseObj.message = "Mobile Number already exist !!";
                    responseObj.result = response.result;
                    next(responseObj);
                }
                else if (response.statusCode === -1){
                    responseObj.error = response.error;
                    next(responseObj);
                }
                else{
                    //console.log("mobile number not found");
                    //Add business entity users in BusinessEntityUserMaster table.
                    var addBusinessEntityUserPromise =  new Promise(function(resolve,reject){
                        BusinessEntityUserMaster.create(businessEntityUsers).exec(function(error,records){
                            if(error){
                                //console.log("user model error",error);
                                responseObj.message = error;
                                sails.log.error("BusinessEntityUserMaster>addBusinessEntityUser>BsusinessEntityUserMaster.create ","req body:",businessEntityUsers," error:"+error);
                                reject();
                            }else if(!records){
                                responseObj.message = "undefined object was returned";
                                sails.log.error("BusinessEntityUserMaster>addBusinessEntityUser>BusinessEntityUserMaster.create ","req body:",businessEntityUsers," error:"+error);
                                reject();
                            }
                            else{
                                //console.log("user model success");
                                // Decryption
                                var i;
                                for(i=0;i<records.length;i++){
                                    records[i].streetAddress = EncryptDecryptService.decrypt(records[i].streetAddress);
                                    records[i].email = EncryptDecryptService.decrypt(records[i].email);
                                    records[i].dateOfBirth = EncryptDecryptService.decrypt(records[i].dateOfBirth);

                                }
                                result = records;
                                sails.log.info("BusinessEntityUserMaster>addBusinessEntityUser>BusinessEntityUserMaster.create : business entity users are added sucessfully");
                                resolve();
                            }
                        });
                    }); 

                    //on success add data to credentials table
                    addBusinessEntityUserPromise.then(function(){
                        // hit the add credentials api
                       // console.log("inside promise");
                        request( requestOptions,function (error, response, body) {
                            //console.log("body is",body,"error is",error);
                            if (!error && body.statusCode === 0) {
                                //console.log("Inside success");
                                responseObj.statusCode = 0;   // api operation succesfully completed
                                responseObj.message = "User has been successfully created !!!!";
                                responseObj.result = result;
                                sails.log.info("BusinessEntityUserMaster>addBusinessEntityUser> Authentication Service :User and its credentials sucessfully created !!!!");
                                next(responseObj);
                            }else {
                                //console.log("Inside error");
                                // delete the added BE user to maintain integrity
                                BusinessEntityUserMaster.deleteBusinessEntityUser(userIdArray,function(response){
                                    if(response.statusCode === 0){
                                        responseObj.message = "User credentails failed";
                                        if(body!==undefined){
                                            responseObj.message = responseObj.message + ": "+body.message;
                                        }
                                    }
                                    else{
                                        responseObj.statusCode = -2;
                                        responseObj.message = response.message;
                                    }
                                    next(responseObj);
                                });
                            }
                        });   // end of api hit

                    },function(){
                        // add BE user error
                        next(responseObj);
                    });

                }   
            }); 

        
    },//End of addBusinessEntityUser function

*/

	//Function to add business entity user
  	addBusinessEntityUser:function(businessEntityUsers,token,next){

  		//Variable declaration
  		var userId = null; // To store userId
  		var timeStamp = null;
        var token;

  		//console.log("user Obj",businessEntityUsersDetails);

         var responseObj = {
            statusCode:-1,
            message:null,
            result:null
        };

        var smsObject;      // req object for sending sms
        var message;        // message to be sent

         //form array to send email
        var emailIds = [];
        var userIds = [];
        
        console.log("creating business staging user",businessEntityUsers);

  
        // generattng unique user ids
        var i;
        for(i=0;i<businessEntityUsers.length;i++){
            timeStamp = new Date().getTime();
            userId = "BU"+timeStamp+i;
            businessEntityUsers[i].userId = userId; 
            //Code for encryption
            if(businessEntityUsers[i].streetAddress){
              businessEntityUsers[i].streetAddress = EncryptDecryptService.encrypt(businessEntityUsers[i].streetAddress);
            }
            if(businessEntityUsers[i].email){
              businessEntityUsers[i].email = EncryptDecryptService.encrypt(businessEntityUsers[i].email);  
            }
            if(businessEntityUsers[i].dateOfBirth){
              businessEntityUsers[i].dateOfBirth = EncryptDecryptService.encrypt(businessEntityUsers[i].dateOfBirth);  
            }  
            businessEntityUsers[i].password = EncryptDecryptService.encrypt(businessEntityUsers[i].password);  
        
        };

        console.log("businessEntityUserDetails",businessEntityUsers);

  		BusinessEntityUserStagingMaster.create(businessEntityUsers).exec(function(error,records){
            console.log("creating business staging user");
            if(error){
                console.log("user model error",error);
                responseObj.message = error;
                sails.log.error("BusinessEntityUserStagingMaster>addBusinessEntityUser>BusinessEntityUserStagingMaster.create ","req body:",businessEntityUsers," error:"+error);
                next(responseObj);
            }else if(!records){
                responseObj.message = "undefined object was returned";
                sails.log.error("BusinessEntityUserStagingMaster>addBusinessEntityUser>BusinessEntityUserStagingMaster.create ","req body:",businessEntityUsers," error:"+error);
                next(responseObj);
            }
            else{
                console.log("user model success");
                // Decryption
                var i;
                var recordsLength = records.length;
                for(i=0;i<recordsLength;i++){
                    if(records[i].streetAddress){
                      records[i].streetAddress = EncryptDecryptService.decrypt(records[i].streetAddress);
                    }
                    if(records[i].email){
                      records[i].email = EncryptDecryptService.decrypt(records[i].email);                      
                    }
                    if(records[i].dateOfBirth){
                      records[i].dateOfBirth = EncryptDecryptService.decrypt(records[i].dateOfBirth);                    
                    }
                    records[i].password = EncryptDecryptService.decrypt(records[i].password);

                    /* SEND SMS TO USERS REGISTERED*/
                    //Ids to be used for sending email
                    emailIds.push(records[i].email);
                    userIds.push(records[i].userId);    

                    //REQUESToBJECT TO SEND SMS
                    var smsInfo = {
                        userId:records[i].userId,
                        mobileNumber:records[i].mobileNumber,
                        message:ServConfigService.getApplicationConfig().transactionalMessages.businessEntityKycRegistration
                    }       

                    /*message =  "You have been registered for Annectos Rewards. Your KYC Verification is completed !!!";
                    smsObject = {mobileNumber:result[i].mobileNumber,message:message};*/

                    //REQUESTOBJECT TO SEND EMAIL
                    var emailInfo = {
                        emailIds: emailIds,
                        userIds: userIds,
                        subject: ServConfigService.getApplicationConfig().transactionalMessages.businessEntityKycRegistration,
                        text: ServConfigService.getApplicationConfig().transactionalMessages.businessEntityKycRegistration 
                    }

                    CommunicationService.sendSms(smsInfo,token);

                    emailIds.push(records[i].email);
                    userIds.push(records[i].userId);    
                    token = JWTService.issue({id: userIds[0]});

                    //REQUESToBJECT TO SEND SMS
                    var smsInfo = {
                        userId:records[i].userId,
                        mobileNumber:records[i].mobileNumber,
                        message:ServConfigService.getApplicationConfig().transactionalMessages.businessEntityKycRegistration
                    }       

                    //REQUESTOBJECT TO SEND EMAIL
                    var emailInfo = {
                        emailIds: emailIds,
                        userIds: userIds,
                        subject: ServConfigService.getApplicationConfig().transactionalMessages.businessEntityKycRegistration,
                        text: ServConfigService.getApplicationConfig().transactionalMessages.businessEntityKycRegistration 
                    }

                    CommunicationService.sendSms(smsInfo,token);

                }
                //Consuming Communication service for sms/email
                CommunicationService.sendEmail(emailInfo,token);

                responseObj.message = "User sucessfully created !!!!";
                responseObj.statusCode = 0;
                responseObj.result = records;
                sails.log.info("BusinessEntityUserStagingMaster>addBusinessEntityUser>BusinessEntityUserStagingMaster.create : business entity users are added sucessfully");
                

                next(responseObj);
            }
        }); 

 	},//End of addBusinessEntityUser function

    updateBusinessEntityUser:function(reqBody,token,next){

        //default responseObj object
        var responseObj = {
           "statusCode": -1,
           "message": null,
           "result": null
        };

        var userInfo = reqBody.userInfo;
        var userId = userInfo.userId;
        var mobileNumber = userInfo.mobileNumber;
        var mobileNumberArray = [] //array of mobileNumber to find check duplicate
        var updateInfo = {}; //variable defined to update EntityStaging table
        var businessId;
        delete userInfo.userId;

        var prevBEUser = {};   //user before updation   
        var userPassword;   // to store new password to be updated

        var errTrack;

        //Encrypting  data
        if(userInfo.streetAddress){
            userInfo.streetAddress = EncryptDecryptService.encrypt(userInfo.streetAddress);
        }
        if(userInfo.email){
            userInfo.email = EncryptDecryptService.encrypt(userInfo.email);            
        }
        if(userInfo.dateOfBirth){
            userInfo.dateOfBirth = EncryptDecryptService.encrypt(userInfo.dateOfBirth);            
        }

        if(userInfo.password){
            userInfo.password = EncryptDecryptService.encrypt(userInfo.password);            
        }

            async.series([        
                function(callback) {     //first function to find the BE User
                    console.log("In F1");
                    var findObject = {userId:userId,isDeleted:false}; 

                    BusinessEntityUserStagingMaster.findBusinessEntityUser(findObject,function(response){
                        if(response.statusCode === 0){
                            console.log("F1 Success:User found ");
                            prevBEUser = response.result[0];
                            businessId = prevBEUser.businessId[0];
                            //console.log("prevBEUser",prevBEUser);
                            callback(null,response);

                        } 
                        else{
                            console.log("F1 Failure: user not found");
                            errTrack = 0;
                            callback("F1 failed",response);                              
                        }
                    });// end of find user
                                   
               },
               //check for duplicate mobileNumber
               function(callback){

                    if(mobileNumber !== undefined){
                        console.log("Mobile function called");
                        mobileNumberArray.push(mobileNumber);
                        // find object for findBusinessEntityUser
                        var findObject = {
                            kycStatus:true,
                            mobileNumberArray:mobileNumberArray
                        };

                        BusinessEntityUserMaster.findBusinessEntityUser(findObject,function(response){
                            //console.log("findUser",response);
                            if(response.statusCode === 0){
                                var updateInfo = {};
                                updateInfo.ismobileNumberAlreadyExist = true;
                                updateInfo.existingBusinessEntityUsers = response.result;

                                //Update record in BusinessEntityStaging table
                                CommonOperations.updateRecord("BusinessEntityStagingMaster","businessId",businessId,updateInfo,token,function(updatedResponse){
                                    console.log("staging response",updatedResponse);
                                    if(updatedResponse.statusCode === 0){
                                        callback(null,'success')
                                    }
                                    else{
                                        responseObj.statusCode = 2;
                                        responseObj.message = "Failed to update in BusinessEntityStaging table";
                                        errTrack = 1;
                                        callback("F2 failed",responseObj);
                                    }
                                })
                            }
                            else if(response.statusCode === -1){
                                responseObj.message = response.error;
                                errTrack = 1;
                                callback("F2 failed",responseObj);
                            }
                            else{
                                callback(null,'success');
                            } 

                        })//end of findUser function
                    }
                    else{
                        callback(null,'success');
                    }
                },

                function(callback) {     //second function to update BE user
                    console.log("In F3 : prevBEUser");
                    var response ={};
                    if(userInfo.isDeleted && prevBEUser.userType === "owner"){
                        console.log("cannot delete user type owner");
                        response.statusCode = 4;
                        response.message = "User type Owner cannot be deleted";
                        errTrack = 2;
                        callback("F3 failed",response);
                    }
                    else{
                        CommonOperations.updateRecord(BusinessEntityUserStagingMaster,"userId",userId,userInfo,token,function(response){
                            if(response.statusCode === 0){
                                responseObj.result = response.result[0];
                                //Decryption
                                if(responseObj.result.dateOfBirth){
                                    responseObj.result.dateOfBirth = EncryptDecryptService.decrypt(responseObj.result.dateOfBirth);
                                }
                                if(responseObj.result.streetAddress){
                                    responseObj.result.streetAddress = EncryptDecryptService.decrypt(responseObj.result.streetAddress);
                                }
                                if(responseObj.result.email){
                                    responseObj.result.email = EncryptDecryptService.decrypt(responseObj.result.email);
                                }
                                callback(null,response);
                            }
                            else{
                                console.log("F3:Failure",response);
                                errTrack = 2;
                                callback("F3 failed",response);
                            }
                        });// end of update
                    }                                   
               }
          ],   // end of array of functions
          function(err,result){
                //console.log("err is", err," results is ",result[errTrack]);
                if (err){
                    //perform rollback for update EntityStagingMaster
                    if(mobileNumber !== undefined){

                        updateInfo.ismobileNumberAlreadyExist = false;
                        updateInfo.existingBusinessEntityUsers = [];
                        //Update record in BusinessEntityStaging table
                        CommonOperations.updateRecord("BusinessEntityStagingMaster","businessId",businessId,updateInfo,token,function(response){

                            if(response.statusCode === 0){
                                console.log("Rollback successfully performed");
                            }
                            else{
                                console.log("Failed to perform rollback for staging master");
                            }
                        })
                    }
                        responseObj.statusCode = result[errTrack].statusCode;
                        responseObj.message = result[errTrack].message;
                    
                    next(responseObj);
               }
               else{
                    responseObj.statusCode = 0;
                    responseObj.message = "User successfully updated";
                    next(responseObj);
               }
 
          }); //End of Async ser


    },



    //To find duplicate mobile number from business entity user master
    findBusinessEntityUser:function(reqBody,next){
       
        //Response object with default value.
        var responseObj = {
            statusCode:-1,
            message:null
        }

        var queryString;
        var mobileNumberArray;


        if (reqBody.userId!==undefined){
            queryString = {userId:reqBody.userId,limit:1};
        }   
        else{
            mobileNumberArray   = reqBody.mobileNumberArray;
            queryString = {mobileNumber:mobileNumberArray}
        }

        //To store the response array.
        responseObj.result=[];

        BusinessEntityUserStagingMaster.find(queryString).exec(function(error,records){
            if(error || records=== undefined){
                if(error){
                    responseObj.message = error;
                    sails.log.error("BusinessEntityUserMaster>findDuplicateNumber>BusinessEntityUserMaster.find ","req body:",reqBody," error:"+error);
                    return next(responseObj);
                }
            }else{
                if(records.length<=0){
                    responseObj.message = "No record found";
                    sails.log.info("BusinessEntityUserMaster>findDuplicateNumber>BusinessEntityUserMaster.find : No duplicate mobile number found");
                    responseObj.statusCode = 2;
                    return next(responseObj);
                }else{
                    // decryption
                    var i;
                    var recordsLength = records.length;
                    for(i=0;i<recordsLength;i++){
                        if(records[i].streetAddress){
                          records[i].streetAddress = EncryptDecryptService.decrypt(records[i].streetAddress);
                        }
                        if(records[i].email){
                          records[i].email = EncryptDecryptService.decrypt(records[i].email);                      
                        }
                        if(records[i].dateOfBirth){
                          records[i].dateOfBirth = EncryptDecryptService.decrypt(records[i].dateOfBirth);                    
                        }              
                    }
                    responseObj.message="Record found";
                    sails.log.info("BusinessEntityUserMaster>findDuplicateNumber>BusinessEntityUserMaster.find : Duplicate mobile number found");
                    responseObj.statusCode = 0;
                    responseObj.result = records;
                    return next(responseObj);
                }
                
            }
        });
    },//End of findDuplicateNumber function



//----------------------------------------------------------------------------------------------------------//

    /*
    # Method: getBusinessEntityUsers
    # Description: to fetch Business Entity users along with their B.E. Info's
    */    


    getBusinessEntityUsers:function(reqBody,token,next){
        //Varibale declaration


        var skip = reqBody.skip;
        var limit = reqBody.limit;
        var sort = reqBody.sort;
        var mobileNumberArray   = reqBody.mobileNumberArray;
        var searchbyUser = reqBody.searchbyUser;

        var queryString = {isDeleted:false,skip:skip,limit:limit,sort:sort};

        var mobileNumberArray;
        if (reqBody.userId!==undefined){
            queryString = {userId:reqBody.userId,isDeleted:false,limit:1};
        }   
        else{
            if(mobileNumberArray){
                queryString = {mobileNumber:mobileNumberArray,isDeleted:false};
            }

            if(searchbyUser){
                queryString.or =[{fullName: new RegExp(searchbyUser,"i")},{mobileNumber:new RegExp(searchbyUser,"i")}];
            }
        }


        //Response object with default value.
        var responseObj = {
            statusCode:-1,
            message:null
        };

        console.log("finding busasSASAASASAASiness entity user",queryString );
        //To store the response array.
        responseObj.result=[];

        BusinessEntityUserStagingMaster.find(queryString).exec(function(error,records){
            console.log("records",records);
            if(error || records=== undefined){
                if(error){
                    responseObj.message = error;
                    sails.log.error("BusinessEntityUserMaster>findDuplicateNumber>BusinessEntityUserMaster.find ","req body:",reqBody," error:"+error);
                    return next(responseObj);
                }
            }else{
                if(records.length<=0){
                    responseObj.message = "No Record found";
                    sails.log.info("BusinessEntityUserMaster>findDuplicateNumber>BusinessEntityUserMaster.find : No duplicate mobile number found");
                    responseObj.statusCode = 2;
                    return next(responseObj);
                }else{
                    responseObj.message="Record found";
                    sails.log.info("BusinessEntityUserMaster>findDuplicateNumber>BusinessEntityUserMaster.find : Duplicate mobile number found");
                    // Decryption
                    var i;
                    var j;

                    var recordsLength = records.length;
                    var businessIdArray = []; // to store all business IDs

                    for(i=0;i<recordsLength;i++){  // decryption
                        if(records[i].streetAddress){
                          records[i].streetAddress = EncryptDecryptService.decrypt(records[i].streetAddress);
                        }
                        if(records[i].email){
                          records[i].email = EncryptDecryptService.decrypt(records[i].email);                      
                        }
                        if(records[i].dateOfBirth){
                          records[i].dateOfBirth = EncryptDecryptService.decrypt(records[i].dateOfBirth);                    
                        }                    
                        for (j=0;j<records[i].businessId.length;j++){   // to store business ID's array
                            businessIdArray.push(records[i].businessId[j]);
                            records[i].businessEntityInfo = [];
                        }
                    }
                    
                        // Get Business Entitiy Info of all business ID's
                        var reqObject = {"businessIdArray":businessIdArray};
                        BusinessEntityStagingMaster.getBusinessEntity(reqObject,token,function(response){
                            if (response.statusCode === 0){
                                // Code to attach BE info's to theire respective users
                                var responseArray = response.result;
                                var responseArraylength = responseArray.length;
                                var tempBusinessId;
                                var tempBusinessIdArray;

                                for(i=0;i<responseArraylength;i++){
                                    tempBusinessId = responseArray[i].businessId;
                                    for (j=0;j<recordsLength;j++){
                                        tempBusinessIdArray = records[j].businessId;
                                        if(tempBusinessIdArray.indexOf(tempBusinessId) > -1){
                                            records[j].businessEntityInfo.push(responseArray[i]);
                                            break;
                                        }
                                    }
                                }
                                // end of code

                                responseObj.statusCode = 0;
                                responseObj.result = records;
                                next(responseObj);
                            }
                            else{
                                responseObj.statusCode = response.statusCode;
                                responseObj.message = response.message;
                                next(responseObj);
                            }
                        });  // end of get business entity
                   
                }   // end of inner else
                
            }// endo of outer else
        });
    },//End of function



//----------------------------------------------------------------------------------------------------------//

    /*
    # Method: loginBusinessEntity
    # Description: to check if user exist in the staging table
    */    


    loginBusinessEntity:function(reqBody,next){
        //Varibale declaration

        
        if(reqBody.loginType !== undefined){
            var queryString = {mobileNumber:reqBody.mobileNumber,isDeleted:false};
        }else{
            var password = EncryptDecryptService.encrypt(reqBody.password);
            var queryString = {mobileNumber:reqBody.mobileNumber,password:password,isDeleted:false};
        }
        

    
        //Response object with default value.
        var responseObj = {
            statusCode:-1,
            message:null
        };

        console.log("loginBusinessEntity",queryString );
        //To store the response array.
        responseObj.result=[];

        BusinessEntityUserStagingMaster.find(queryString).exec(function(error,records){
            console.log("records",records);
            if(error || records=== undefined){
                if(error){
                    responseObj.message = error;
                    sails.log.error("BusinessEntityUserMaster>findDuplicateNumber>BusinessEntityUserMaster.find ","req body:",reqBody," error:"+error);
                    return next(responseObj);
                }
            }else{
                if(records.length<=0){
                    responseObj.message = "No Record found";
                    sails.log.info("BusinessEntityUserMaster>findDuplicateNumber>BusinessEntityUserMaster.find : No duplicate mobile number found");
                    responseObj.statusCode = 2;
                    return next(responseObj);
                }else{
                    responseObj.message="Record found";
                    sails.log.info("BusinessEntityUserMaster>findDuplicateNumber>BusinessEntityUserMaster.find : Duplicate mobile number found");
                    // Decryption
                    var i;
                    var j;

                    var recordsLength = records.length;
                    var businessIdArray = []; // to store all business IDs

                    for(i=0;i<recordsLength;i++){  // decryption
                        if(records[i].streetAddress){
                          records[i].streetAddress = EncryptDecryptService.decrypt(records[i].streetAddress);
                        }
                        if(records[i].email){
                          records[i].email = EncryptDecryptService.decrypt(records[i].email);                      
                        }
                        if(records[i].dateOfBirth){
                          records[i].dateOfBirth = EncryptDecryptService.decrypt(records[i].dateOfBirth);                    
                        }                    
                        for (j=0;j<records[i].businessId.length;j++){   // to store business ID's array
                            businessIdArray.push(records[i].businessId[j]);
                            records[i].businessEntityInfo = [];
                        }
                    }
                    
                            responseObj.statusCode = 0;
                            responseObj.result = records;
                            next(responseObj);
                    
                }   // end of inner else
                
            }// endo of outer else
        });
    }//End of function


};







