/**
 * States.js
 *
 * @description     :: To add, update and delete the business entities.
 * @createdBy       :: Nilesh
 * @created Date    :: 28-12-2016
 * @Last edited by  :: Nilesh
 * @last edited date::
 */


/**** Status code

    0  => success
    -1 => Error
    1  => Authentication failed
    2  => Data not found
    3  => Duplicate value exist.

****/

var promise = require('promise');
var async = require('async');
var request = require('request');

// code to accept self-signed certifcate
if ('development' == sails.config.environment) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
};


module.exports = {

	//Table name and attributes of table
  	tableName:'BusinessEntityUserMaster',
  	attributes: {
  		businessId:{
  			type:'array',
			required:true
  		},
  		fullName:{
  			type:'string',
  			required:true
  		},
  		firstName:{
  			type:'string',
  			required:true
  		},
  		lastName:{
  			type:'string',
  			required:true
  		},
  		userId:{
  			type:'string',
			primaryKey:true,
			unique:true,
			required:true
  		},
  		mobileNumber:{
  			type:'string',
  			required:true,
  		},
  		email:{
  			type:'string',
  			required:true,
  		},
  		dateOfBirth:{
  			type:'string'/*,
  			required:true*/
  		},
        gender:{
            type:'string',
            required:true
        },
  		state:{
  			type:'string',
  			required:true
  		},
  		city:{
  			type:'string',
  			required:true
  		},
        landmark:{
            type:'string'
        },
        area:{
            type:'string'
        },
        streetAddress:{
            type:'string',
            required:true
        },
  		pincode:{
  			type:'integer',
  			required:true
  		},
  		userType:{
  			type:'string',
            enum:['owner','Family member','Delegate'],
  			required:true
  		},
  		marriageAnniversary:{
  			type:'string',
  		},
  		maritalStatus:{
  			type:'string'
  		},
        /* fields added later*/
        noOfChildrens:{
            type:'integer'
        },
        ageOfChildren:{
            type:'string'
        },
        preferredLanguage:{
            type:'string'
        },
        idProof:{
            type:'string'
        },
        /* end */
  		createdBy:{
			type:'string'
		},
		updatedBy:{
			type:'string'
		},
  	    isDeleted:{
  			type:'boolean',
  			required:true,
  			defaultsTo:false
  	    },
        alternateMobileNumber:{
            type:'array'
        }
  	},

	//Function to add business entity user (single user) and its credentials
  	addBusinessEntityUser:function(businessEntityUsers,token,next){

  		//Variable declaration
  		var userId = null;
  		var timeStamp = null;
        var queryString = [];
        var result;     // to store result
        var loginObj;
        var requestApi;
        var requestOptions;


        // Response object with default value.
        var responseObj = {
            statusCode:-1,
            message:null,
            result:null
        };


        var i;
        //form array to send email
        var emailIds = [];
        var userIds = [];

        //console.log("Business entity users:",businessEntityUsers);
  		 
        var userCredentials = [];  // to create array of credentials object (Credentials Service)
        var tempObject;            
        var userIdArray = [];   // array of user ids
        var mobileNumberArray = [];   // array of mobile numbers to find
        var migrate = false;
        var messages = [] //array of messages to send for businessEntity

        var businessEntityUsersLength = businessEntityUsers.length;
        messages.length = businessEntityUsersLength;
        // generate user ids and credentials objaect
        // encryption
        for(i=0;i<businessEntityUsersLength;i++){
            // for user id
            console.log("inside loop",i);

            if(businessEntityUsers[i].userId === undefined){       // if not migrating
                timeStamp = new Date().getTime();
                userId = "BU"+timeStamp+i;
                businessEntityUsers[i].userId = userId;   
            }
            else{
                userId = businessEntityUsers[i].userId;
                migrate = true
            }               
            userIdArray.push(userId)

            // add credentials object
            tempObject = {
                userId: userId,
                userName: businessEntityUsers[i].mobileNumber,
                password: businessEntityUsers[i].password,
                userType: "BusinessEntityUser"
            };
            userCredentials.push(tempObject);

            //emailIds.push(businessEntityUsers[i].email);
            userIds.push(businessEntityUsers[i].userId);  

            messages[i] = ServConfigService.getApplicationConfig().transactionalMessages.businessEntityKycRegistration,
            messages[i] = messages[i].replace("<u>",businessEntityUsers[i].email);
            messages[i] = messages[i].replace("<p>",businessEntityUsers[i].password);
            // to check if mobile number already exists
            mobileNumberArray.push(businessEntityUsers[i].mobileNumber);    

            delete businessEntityUsers[i].password;   // password is not stored over here

            //Code for encryption
            businessEntityUsers[i].streetAddress = EncryptDecryptService.encrypt(businessEntityUsers[i].streetAddress);
            businessEntityUsers[i].email = EncryptDecryptService.encrypt(businessEntityUsers[i].email);
            if(businessEntityUsers[i].dateOfBirth){
                businessEntityUsers[i].dateOfBirth = EncryptDecryptService.encrypt(businessEntityUsers[i].dateOfBirth);
            }
            
            console.log("encrypt",businessEntityUsers[i].dateOfBirth); 

        };  // end of for loop

                        console.log("output log",i);

        //variable defined to form requestApi
        requestApi  = ServConfigService.getApplicationConfig().base_url
                        + ":"
                        + ServConfigService.getApplicationAPIs().addUserCredentials.port
                        + ServConfigService.getApplicationAPIs().addUserCredentials.url;

        ////console.log("requestApi is",requestApi,"and userCredentials",userCredentials);

        requestOptions = {
                    url: requestApi, //ServConfigService.getUserPersonalDetailsAPI.url+"?userId="+foundCredentials.userId,
                    method: ServConfigService.getApplicationAPIs().addUserCredentials.method,
                    headers: {
                        'authorization' : 'Bearer ' + token
                    },
                    json:userCredentials
        };
        
        //console.log("requestOptions is",requestOptions);


        ////console.log("mobileNumberArray",mobileNumberArray);
        // find object for findBusinessEntityUser
        var findObject = {
            kycStatus:true,
            mobileNumberArray:mobileNumberArray
        };

            // Check if mobile number already exists
            BusinessEntityUserMaster.findBusinessEntityUser(findObject,function(response){
                if(response.statusCode === 0){
                    ////console.log("mobile number found",response);
                    responseObj.statusCode = 3;
                    responseObj.message = "Mobile Number already exist !!";
                    responseObj.result = response.result;
                    next(responseObj);
                }
                else if (response.statusCode === -1){
                    responseObj.error = response.error;
                    next(responseObj);
                }
                else{
                    ////console.log("mobile number not found");
                    //Add business entity users in BusinessEntityUserMaster table.
                    var addBusinessEntityUserPromise =  new Promise(function(resolve,reject){
                        BusinessEntityUserMaster.create(businessEntityUsers).exec(function(error,records){
                            if(error){
                                ////console.log("user model error",error);
                                responseObj.message = error;
                                sails.log.error("BusinessEntityUserMaster>addBusinessEntityUser>BsusinessEntityUserMaster.create ","req body:",businessEntityUsers," error:"+error);
                                reject();
                            }else if(!records){
                                responseObj.message = "undefined object was returned";
                                sails.log.error("BusinessEntityUserMaster>addBusinessEntityUser>BusinessEntityUserMaster.create ","req body:",businessEntityUsers," error:"+error);
                                reject();
                            }
                            else{
                                ////console.log("user model success");
                                // Decryption
                                for(i=0;i<records.length;i++){
                                    records[i].streetAddress = EncryptDecryptService.decrypt(records[i].streetAddress);
                                    records[i].email = EncryptDecryptService.decrypt(records[i].email);
                                    if(records[i].dateOfBirth){
                                        records[i].dateOfBirth = EncryptDecryptService.decrypt(records[i].dateOfBirth);
                                    }
                                }
                                result = records;
                                sails.log.info("BusinessEntityUserMaster>addBusinessEntityUser>BusinessEntityUserMaster.create : business entity users are added sucessfully");
                                resolve();
                            }
                        });
                    }); 

                    //on success add data to credentials table
                    addBusinessEntityUserPromise.then(function(){
                        // hit the add credentials api
                       // //console.log("inside promise");
                        request( requestOptions,function (error, response, body) {
                            //console.log("body is",body,"error is",error);
                            if (!error && body) {
                                ////console.log("Inside success");
                                if(body.statusCode === 0){
                                    responseObj.statusCode = 0;   // api operation succesfully completed
                                    responseObj.message = "User has been successfully created !!!!";
                                    responseObj.result = result;

                                    
                                     /* SEND SMS TO USERS REGISTERED*/
                                    if(migrate === false){

                                        for(i=0;i<result.length;i++){     

                                            emailIds = [];  
                                            userId = [];            
                                            
                                           
                                            //REQUESTOBJECT TO SEND SMS
                                            for(var j=0; j<userIds.length; j++){

                                                if(userIds[j] === result[i].userId){

                                                    emailIds.push(result[i].email);
                                                    userId.push(result[i].userId);

                                                    var smsInfo = {
                                                        userId:userIds[j],
                                                        mobileNumber:mobileNumberArray[j],
                                                        message:messages[j]
                                                    } 

                                                    var emailInfo = {
                                                        emailIds: emailIds,
                                                        userIds: userId,
                                                        subject: messages[j],
                                                        text: messages[j]
                                                    }

                                                    //Consuming Communication service for sms/email
                                                    CommunicationService.sendSms(smsInfo,token);  
                                                    CommunicationService.sendEmail(emailInfo,token);
                                                }
                                                
                                            }
                                        }
                                    }
                                    /*--------------------------*/

                                    sails.log.info("BusinessEntityUserMaster>addBusinessEntityUser> Authentication Service :User and its credentials sucessfully created !!!!");
                                    next(responseObj);
                                }
                                else{
                                          // delete the added BE user to maintain integrity
                                    BusinessEntityUserMaster.deleteBusinessEntityUser(userIdArray,function(response){
                                        if(response.statusCode === 0){
                                                responseObj.message = responseObj.message + ": "+body.message;
                                            
                                        }
                                        else{
                                            responseObj.statusCode = -2;
                                            responseObj.message = "Integrity Error: Failed to delete Business entity users";
                                            responseObj.result = userIdArray;
                                        }
                                        next(responseObj);
                                    });
                                }    
                            }else {
                                ////console.log("Inside error");
                                // delete the added BE user to maintain integrity
                                BusinessEntityUserMaster.deleteBusinessEntityUser(userIdArray,function(response){
                                    if(response.statusCode === 0){
                                        responseObj.message = "User Credentials Service failed:"+ error;
                                        if(body!==undefined){
                                            responseObj.message = responseObj.message + ": "+body.message;
                                        }
                                    }
                                    else{
                                        responseObj.statusCode = -2;
                                            responseObj.message = "Integrity Error: Failed to delete Business entity users";
                                            responseObj.result = userIdArray;
                                    }
                                    next(responseObj);
                                });
                            }
                        });   // end of api hit

                    },function(){
                        // add BE user error
                        next(responseObj);
                    });

                }   
            }); 

        


 	},//End of addBusinessEntityUser function

    //delete the business entity user
    deleteBusinessEntityUser:function(userId,next){
        var responseObj = {
            statusCode:-1,
            message:null,
            result:null
        };

        ////console.log("userId array is",userId);

        BusinessEntityUserMaster.destroy({"userId":userId}).exec(function(error,deletedRecord){
            if(error || !deletedRecord){
                ////console.log("unable to destroy record");
                responseObj.statusCode = -2;
                responseObj.message = "Contact Admin : Business entity created but failed to create user";
                responseObj.result = userId;
                sails.log.error("BusinessEntityUserMaster>deleteBusinessEntityUser>BusinessEntityUserMaster.destroy ","req body:"+businessEntityDetails," error:"+error);
                return next(responseObj);
            }else{
                if(deletedRecord.length === 0){
                     responseObj.statusCode = -2;
                    responseObj.message = "Contact Admin : Business entity created but failed to create user";
                    responseObj.result = userId;
                    sails.log.error("BusinessEntityUserMaster>deleteBusinessEntityUser>BusinessEntityUserMaster.destroy ","req body:"+userId," error:"+error);
                    return next(responseObj);
                }
                else{
                    ////console.log("deletedRecord",deletedRecord);
                    responseObj.statusCode = 0;
                    responseObj.message = "Record successfully deleted";
                    sails.log.info("BusinessEntityUserMaster>deleteBusinessEntityUser>BusinessEntityUserMaster.destroy: record is successfully destroyed");
                    return next(responseObj);
                }
            }        
        });
    },



     updateBusinessEntityUser:function(reqBody,token,next){

          //default responseObj object
            var responseObj = {
               "statusCode": -1,
               "message": null,
               "result": null
            };

          var userInfo = reqBody.userInfo;
          var userId = userInfo.userId;
          delete userInfo.userId;

          var prevBEUser = {};   //user before updation   
          var userPassword;   // to store new password to be updated


          var findExistingNumber =  false;        //flag for function 2
          var updateUserCredentials = false;      // flag for function 4
          var userCredentialsObject = {};

          var revertUpdatedUser = false;     // to revert back updated BE user
          var errTrack;

          if(userInfo.isDeleted){                 // delete operation to be performed
               updateUserCredentials = true;
               userCredentialsObject.isDeleted = true;
          }
          else{
               if(userInfo.mobileNumber){
                    userCredentialsObject.userName = userInfo.mobileNumber;
                    findExistingNumber = true;
                    updateUserCredentials = true;
               }
               if(userInfo.password){
                    userCredentialsObject.password = EncryptDecryptService.encrypt(userInfo.password);
                    delete userInfo.password;
                    updateUserCredentials = true;
               }
          } 

        //Encrypting  data
        if(userInfo.streetAddress){
            userInfo.streetAddress = EncryptDecryptService.encrypt(userInfo.streetAddress);
        }
        if(userInfo.email){
            userInfo.email = EncryptDecryptService.encrypt(userInfo.email);            
        }
        if(userInfo.dateOfBirth){
            userInfo.dateOfBirth = EncryptDecryptService.encrypt(userInfo.dateOfBirth);                       
        }


          async.series([        
               function(callback) {     //first function to find the BE User
                    //console.log("In F1");
                    var findObject = {userId:userId,isDeleted:false}; 

                    BusinessEntityUserMaster.findBusinessEntityUser(findObject,function(response){
                         if(response.statusCode === 0){
                              //console.log("F1 Success:User found ");
                              prevBEUser = response.result[0];
                              ////console.log("prevBEUser",prevBEUser);
                              callback(null,response);

                         } 
                         else{
                              //console.log("F1 Failure: user not found");
                              errTrack = 0;
                              callback("F1 failed",response);                              
                         }
                    });// end of find user
                                   
               },
               function(callback) {     //second function to check if Mobile No Already Exist
                    //console.log("In F2");
                    if(findExistingNumber){
                         var findObject = {mobileNumberArray:[userInfo.mobileNumber]};
                         BusinessEntityUserMaster.findBusinessEntityUser(findObject,function(response){
                              if(response.statusCode === 2){
                                   //console.log("F2 Success:Mobile No Does not exist",response);
                                   callback(null,response);
                              }
                              else if(response.statusCode === 0){
                                   //console.log("F2 Failure:mobile number found");
                                   errTrack = 1;
                                   response.message = "Mobile Number already exists";
                                   callback("F1 failed",response);  
                              }
                              else{
                                   //console.log("F2 Failure: error not found");
                                   errTrack = 1;
                                   callback("F2 failed",response);                              
                              }
                         });// end of find user
                    }
                    else{
                         callback(null,"skip");   // skip to next function 
                    }
                                   
               },
               function(callback) {     //third function to delete BE
                    //console.log("In F3 : prevBEUser",prevBEUser.userType);
                    var response ={};
                    if(userInfo.isDeleted && prevBEUser.userType === "owner"){
                         //console.log("cannot delete user type owner");
                         response.statusCode = 4;
                         response.message = "User type Owner cannot be deleted";
                         errTrack = 2;
                         callback("F3 failed",response);
                    }
                    else{
                         CommonOperations.updateRecord(BusinessEntityUserMaster,"userId",[userId],userInfo,token,function(response){
                              if(response.statusCode === 0){
                                   //console.log("F3:Success",response);
                                   responseObj.result = response.result[0];
                                   //Decryption
                                   if(responseObj.result.dateOfBirth){
                                        responseObj.result.dateOfBirth = EncryptDecryptService.decrypt(responseObj.result.dateOfBirth);
                                   }
                                   if(responseObj.result.streetAddress){
                                        responseObj.result.streetAddress = EncryptDecryptService.decrypt(responseObj.result.streetAddress);
                                   }
                                   if(responseObj.result.email){
                                        responseObj.result.email = EncryptDecryptService.decrypt(responseObj.result.email);
                                   }

                                   callback(null,response);
                              }
                              else{
                                   //console.log("F3:Failure",response);
                                   errTrack = 2;
                                   callback("F3 failed",response);
                              }
                         });// end of update
                    }

                                   
               },
               function(callback) {     //fourth function to update User credentials
                    //console.log("In F4");
      
                    if(updateUserCredentials){
                         CommonOperations.updateRecord(Credentials,"userId",[userId],userCredentialsObject,token,function(response){
                              if(response.statusCode === 0){
                                   //console.log("F4:Success",response);
                                   callback(null,response);
                              }
                              else{
                                   //console.log("F4:Failure",response);
                                   revertUpdatedUser = true;
                                   errTrack = 3;
                                   callback("F4 failed",response);
                              }
                         });// end of update 
                    }
                    else{
                         callback(null,"skip");
                    }                  
               }
          ],   // end of array of functions
          function(err,result){
               //console.log("err is", err," results is ",result);
               if (err){
                    responseObj.statusCode = result[errTrack].statusCode;
                    responseObj.message = result[errTrack].message;
                    if(revertUpdatedUser){   // rollback updated BE user
                         //console.log("REVERTING and prevBEUser");
                         delete prevBEUser.userId;
                         CommonOperations.updateRecord(BusinessEntityUserMaster,"userId",[userId],prevBEUser,token,function(response){
                              if(response.statusCode === 0){
                                   //console.log("R1:Success",response);
                              }
                              else{
                                   responseObj.statusCode = -2;
                                   responseObj.message = "Fatal Integrity Error: User unable to revert back to previous state";
                                   responseObj.result = prevBEUser;
                                   //console.log("R1:Failure",response);
                              }
                              next(responseObj);
                         });// end of update 
                    }    
                    else{     // rollback not required
                         next(responseObj);
                    }
               }
               else{
                    responseObj.statusCode = 0;
                    responseObj.message = "User successfully updated";
                    if(userInfo.isDeleted){
                        responseObj.message = "User successfully deleted";                        
                    }
                    next(responseObj);
               }
 
          }); //End of Async series
    },


    //To find duplicate mobile number from business entity user master
    findBusinessEntityUser:function(reqBody,next){
        //Varibale declaration
        var queryString;
        var mobileNumberArray;
        if (reqBody.userId!==undefined){
            queryString = {userId:reqBody.userId,isDeleted:false,limit:1};
        }   
        else{
            mobileNumberArray   = reqBody.mobileNumberArray;
            queryString = {mobileNumber:mobileNumberArray,isDeleted:false}
        }

        //Response object with default value.
        var responseObj = {
            statusCode:-1,
            message:null
        };

        //console.log("finding business entity user");
        //To store the response array.
        responseObj.result=[];

        BusinessEntityUserMaster.find(queryString).exec(function(error,records){
            if(error || records=== undefined){
                if(error){
                    responseObj.message = error;
                    sails.log.error("BusinessEntityUserMaster>findDuplicateNumber>BusinessEntityUserMaster.find ","req body:",reqBody," error:"+error);
                    return next(responseObj);
                }
            }else{
                if(records.length<=0){
                    responseObj.message = "No Record found";
                    sails.log.info("BusinessEntityUserMaster>findDuplicateNumber>BusinessEntityUserMaster.find : No duplicate mobile number found");
                    responseObj.statusCode = 2;
                    return next(responseObj);
                }else{
                    responseObj.message="Record found";
                    sails.log.info("BusinessEntityUserMaster>findDuplicateNumber>BusinessEntityUserMaster.find : Duplicate mobile number found");
                    // Decryption
                    var i;
                    var recordsLength = records.length;
                    for(i=0;i<recordsLength;i++){
                        records[i].streetAddress = EncryptDecryptService.decrypt(records[i].streetAddress);
                        records[i].email = EncryptDecryptService.decrypt(records[i].email);
                        if(records[i].dateOfBirth){
                            records[i].dateOfBirth = EncryptDecryptService.decrypt(records[i].dateOfBirth);                    
                        }
                    }
                    responseObj.statusCode = 0;
                    responseObj.result = records;
                    return next(responseObj);
                }
                
            }
        });
    },//End of findDuplicateNumber function

//----------------------------------------------------------------------------------------------------------//

    /*
    # Method: getBusinessEntityUsers
    # Description: to fetch Business Entity users along with their B.E. Info's
    */    


    getBusinessEntityUsers:function(reqBody,token,next){
        //Varibale declaration


        var skip = reqBody.skip;
        var limit = reqBody.limit;
        var sort = reqBody.sort;
        var mobileNumberArray   = reqBody.mobileNumberArray;
        var searchbyUser = reqBody.searchbyUser;


        var queryString = {isDeleted:false,skip:skip,limit:limit,sort:sort};

        var mobileNumberArray;
        if (reqBody.userId!==undefined){
            queryString = {userId:reqBody.userId,isDeleted:false,limit:1};
        }   
        else{
            if(mobileNumberArray){
                queryString = {mobileNumber:mobileNumberArray,isDeleted:false};
            }
            if(searchbyUser){
                queryString.or =[{fullName: new RegExp(searchbyUser,"i")},{mobileNumber:new RegExp(searchbyUser,"i")}];
            }
        }

        //Response object with default value.
        var responseObj = {
            statusCode:-1,
            message:null
        };

        //console.log("finding business entity user");
        //To store the response array.
        responseObj.result=[];

        BusinessEntityUserMaster.find(queryString).exec(function(error,records){
            if(error || records=== undefined){
                if(error){
                    responseObj.message = error;
                    sails.log.error("BusinessEntityUserMaster>findDuplicateNumber>BusinessEntityUserMaster.find ","req body:",reqBody," error:"+error);
                    return next(responseObj);
                }
            }else{
                if(records.length<=0){
                    responseObj.message = "No Record found";
                    sails.log.info("BusinessEntityUserMaster>findDuplicateNumber>BusinessEntityUserMaster.find : No duplicate mobile number found");
                    responseObj.statusCode = 2;
                    return next(responseObj);
                }else{
                    responseObj.message="Record found";
                    sails.log.info("BusinessEntityUserMaster>findDuplicateNumber>BusinessEntityUserMaster.find : Duplicate mobile number found");
                    // Decryption
                    var i;
                    var j;

                    var recordsLength = records.length;
                    var businessIdArray = []; // to store all business IDs

                    for(i=0;i<recordsLength;i++){  // decryption
                        records[i].streetAddress = EncryptDecryptService.decrypt(records[i].streetAddress);
                        records[i].email = EncryptDecryptService.decrypt(records[i].email);
                        if(records[i].dateOfBirth){
                            records[i].dateOfBirth = EncryptDecryptService.decrypt(records[i].dateOfBirth);                    
                        }
                        for (j=0;j<records[i].businessId.length;j++){   // to store business ID's array
                            businessIdArray.push(records[i].businessId[j]);
                            records[i].businessEntityInfo = [];
                        }
                    }
                    

                    // Get Business Entitiy Info of all business ID's
                    var reqObject = {"businessIdArray":businessIdArray,skip:skip};
                    BusinessEntityMaster.getBusinessEntity(reqObject,token,function(response){
                        if (response.statusCode === 0){
                            // Code to attach BE info's to theire respective users
                            var responseArray = response.result;
                            var responseArraylength = responseArray.length;
                            var tempBusinessId;
                            var tempBusinessIdArray;

                            for(i=0;i<responseArraylength;i++){
                                tempBusinessId = responseArray[i].businessId;
                                for (j=0;j<recordsLength;j++){
                                    tempBusinessIdArray = records[j].businessId;
                                    if(tempBusinessIdArray.indexOf(tempBusinessId) > -1){
                                        records[j].businessEntityInfo.push(responseArray[i]);
                                        break;
                                    }
                                }
                            }
                            console.log("response from BE",response.count);
                            // end of code
                            if(response.count){
                                responseObj.count = response.count;
                            }

                            responseObj.statusCode = 0;
                            if (reqBody.userId!==undefined){
                                responseObj.result = records[0];                
                            }  
                            else{
                                responseObj.result = records;
                            }
                            next(responseObj);
                        }
                        else{
                            responseObj.statusCode = response.statusCode;
                            responseObj.message = response.message;
                            next(responseObj);
                        }
                    });  // end of get business entity

                }   // end of inner else
                
            }// endo of outer else
        });
    },//End of function



};
