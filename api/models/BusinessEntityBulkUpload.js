/**
 * BusinessEntityBulkUpload.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 * @createdBy       :: Ajeet
 * @created Date    :: 28-01-2017
 */

"use strict";


var async = require('async');
var request = require('request');

// code to accept self-signed certifcate
if ('development' == sails.config.environment) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
};

module.exports = {

  	attributes: {

  	},

  	/*
	# Method: addBusinessEntityBulk
	# Description: to add Business Entity in Bulk
	# Input: 
	# Output : response with success or error
  	*/ 

  	addBusinessEntityBulk:function(reqBody,token,next){

  		//default responseObj
  		var responseObj ={
  			statusCode:-1,
  			message:null,
  			successResult:[],		// store records that are inserted
  			errorResult:[]			// store records that failed to insert
  		};

        var createdBy = reqBody.createdBy;
        var frontendUserInfo = reqBody.frontendUserInfo;
        var dataSource = reqBody.dataSource;
        var kycStatus =  reqBody.kycStatus;

        var businessEntitiesArray = []; // this will contain array of business entites along with their users


        //var path = "BusinessEntityBulkUpload.xlsx";
        // call convert convertExcel2JSON method

        ExcelToJsonService.convertExcelToJson(reqBody,function(response){
          console.log(response.result.length);
        	if(response.statusCode === 0){
        		 // Format records to be inserted
			   	 businessEntitiesArray = BusinessEntityBulkUpload.formatBulkData(response.result,kycStatus,createdBy,dataSource,frontendUserInfo,responseObj);
			     if(kycStatus === true){
			     	BusinessEntityBulkUpload.addBusinessEntitiesMain(businessEntitiesArray,token,responseObj,next);
			     }
			     else if (kycStatus === false){
			     	BusinessEntityBulkUpload.addBusinessEntitiesStaging(businessEntitiesArray,token,responseObj,next);
			     }
			     else{
			     	responseObj.message = "Kyc Status should be either true or false";
		      		next(responseObj);
			     }
        	}
        	else{
        		responseObj.statusCode = response.statusCode;
        		responseObj.message =  response.message;
        		next(responseObj);
        	}
        });

  	},


    /*
    # Method: formatBulkData
    # Description: to convert bulk data into proper format for adding to database
    # Input: 
    # Output : response with success or error
    */

  	formatBulkData:function(inputArray,kycStatus,createdBy,dataSource,frontendUserInfo,responseObj){
  		var i;

  		var result = inputArray;
  		var resultLength = result.length;
        var businessEntitiesArray = []; // this will contain array of business entites along with their users

		for(i=0;i<resultLength;i++){

            var errorObject={};  //default error object

			var businessEntityObject = {};
			businessEntityObject.businessEntityDetails = {};
			businessEntityObject.businessEntityUsers = [];
			var singleUser = {};

			businessEntityObject.kycStatus = kycStatus;
			businessEntityObject.frontendUserInfo = frontendUserInfo;

			businessEntityObject.SrNo = result[i].SrNo;

			businessEntityObject.businessEntityDetails.businessName = result[i].businessName;
			businessEntityObject.businessEntityDetails.state = result[i].state;
			businessEntityObject.businessEntityDetails.city = result[i].city;
			businessEntityObject.businessEntityDetails.area = result[i].area;
			businessEntityObject.businessEntityDetails.landmark = result[i].landmark;
			businessEntityObject.businessEntityDetails.streetAddress = result[i].streetAddress;
			businessEntityObject.businessEntityDetails.pincode = result[i].pincode;
			businessEntityObject.businessEntityDetails.yearOfEstablishment = result[i].yearOfEstablishment;
			businessEntityObject.businessEntityDetails.dealsWithBrands = result[i].dealsWithBrands.split(",");
			businessEntityObject.businessEntityDetails.registrationNumber = result[i].registrationNumber;
			businessEntityObject.businessEntityDetails.monthlyTurnOver = result[i].monthlyTurnOver;
			businessEntityObject.businessEntityDetails.approxShopSize = result[i].approxShopSize;
			businessEntityObject.businessEntityDetails.createdBy = createdBy;
			businessEntityObject.businessEntityDetails.dataSource = dataSource;
            businessEntityObject.businessEntityDetails.type = "BulkUpload";
			singleUser.firstName = result[i].userFirstName;
			singleUser.lastName = result[i].userLastName;
			singleUser.fullName = result[i].userFullName;
			singleUser.state = result[i].userState;
			singleUser.city = result[i].userCity;
			singleUser.area = result[i].userArea;
			singleUser.landmark = result[i].userLandmark;
			singleUser.streetAddress = result[i].userStreetAddress;
			singleUser.pincode = result[i].userPincode;
			singleUser.mobileNumber = result[i].userMobileNumber;
			singleUser.email = result[i].userEmail;
			singleUser.gender = result[i].userGender;

            if(result[i].userDateOfBirth){
                singleUser.dateOfBirth = new Date(result[i].userDateOfBirth);
                if(singleUser.dateOfBirth == "Invalid Date"){
                    console.log("Birthdate isinvalid");
                    errorObject.reason = "Invalid BirthDate";
                    errorObject.failedValue = businessEntitiesArray[i];
                    responseObj.errorResult.push(errorObject);
                    break;
                }else{
                    singleUser.dateOfBirth = singleUser.dateOfBirth.toISOString();
                }
            }

            if(result[i].userMarriageAnniversary){
                singleUser.marriageAnniversary = new Date(result[i].userMarriageAnniversary);
                if(singleUser.marriageAnniversary == "Invalid Date"){
                    console.log("Anniversary is invalid");
                    errorObject.reason = "Invalid Anniversary";
                    errorObject.failedValue = businessEntitiesArray[i];
                    responseObj.errorResult.push(errorObject);
                    break;
                }else{
                    singleUser.marriageAnniversary = singleUser.marriageAnniversary.toISOString();
                }
            }
            /*if(result[i].userDateOfBirth){
                singleUser.dateOfBirth = new Date(result[i].userDateOfBirth);

                console.log("DOB",singleUser.dateOfBirth,"typeof",typeof(singleUser.dateOfBirth));
                singleUser.dateOfBirth = singleUser.dateOfBirth.toISOString();
            }

            if(result[i].userMarriageAnniversary){
                singleUser.marriageAnniversary = new Date(result[i].userMarriageAnniversary);
                console.log("Marriage",singleUser.marriageAnniversary);
                singleUser.marriageAnniversary = singleUser.marriageAnniversary.toISOString();
            }
*/
            console.log("Date",result[i].userDateOfBirth);
            console.log("Anniversary",result[i].userMarriageAnniversary);

            console.log("DOB",singleUser.dateOfBirth,typeof(singleUser.dateOfBirth));
            console.log("Marriage",singleUser.marriageAnniversary,typeof(singleUser.marriageAnniversary));

			singleUser.password = result[i].userPassword;			   		
			singleUser.userType = 'owner';
			singleUser.maritalStatus = result[i].userMaritalStatus;
			singleUser.alternateMobileNumber = result[i].userAlternateMobileNo.split(",");
			singleUser.createdBy = createdBy;
			singleUser.dataSource = dataSource;

			businessEntityObject.businessEntityUsers.push(singleUser);
			businessEntitiesArray.push(businessEntityObject);
		}

		console.log("After formating:",businessEntitiesArray.length);	
		return businessEntitiesArray;
	
  	},

    /*
    # Method: validateBusienssEntityStateCity
    # Description: to validate state and city of businessEntity and it's user
    # Input: 
    # Output : response with success or error
    */

    validateBusienssEntitiesStateCity:function(businessEntitiesArray,responseObj,next){

        

        var states = [] ;     //store array of states
        var cities = [] ;    //store array of cities
        var records = [] ;    //record of states/cities

        var validBusinessEntites = [];   //record having valid cities 

        var isValidEntity = false     //flag set to true when entity is validated

        //requestObject for fetching all sates
        var apiURL  = ServConfigService.getApplicationConfig().base_url
                    + ":"
                    + ServConfigService.getApplicationAPIs().getAllStates.port
                    + ServConfigService.getApplicationAPIs().getAllStates.url;

        var requestOptions= {
            url:apiURL,
            method:ServConfigService.getApplicationAPIs().getAllStates.method,
            json:{}
        }

        //requestObject for fetching all cities
        var apiURL1  = ServConfigService.getApplicationConfig().base_url
                    + ":"
                    + ServConfigService.getApplicationAPIs().getAllCities.port
                    + ServConfigService.getApplicationAPIs().getAllCities.url;

        var requestOptions1 = {
            url:apiURL1,
            method:ServConfigService.getApplicationAPIs().getAllCities.method,
            json:{}
        }

        async.series([

            //fetching all states
            function(callback){

                request(requestOptions,function(error,response,body){
                    //console.log("stateBody",body);
                    if(error || body === undefined){
                        responseObj.message = "Error occurred while fetching states";
                        callback("Error","STATESERROR");
                    }
                    else{
                        if(body.statusCode === 0){
                            records = body.result;
                            //store array of states
                            for(var i=0; i<records.length; i++){
                                states.push(records[i].stateName)
                            }
                            callback(null,"function 1 success");
                        }
                        else{
                            responseObj.message = "States not found";
                            callback("Error","STATESERROR");
                        }
                    }
                })
            },

            //fetch all cities
            function(callback){
                
                request(requestOptions1,function(error,response,body){
                    //console.log("cityBody",body);
                    if(error || body === undefined){
                        responseObj.message = "Error occurred while fetching cities";
                        callback("Error","CITIESERROR");
                    }
                    else{
                        if(body.statusCode === 0){
                            records = body.result;
                            //store array of states
                            for(var i=0; i<records.length; i++){
                                cities.push(records[i].cityName)
                            }
                            callback(null,"function 1 success");
                        }
                        else{
                            responseObj.message = "Cities not found";
                            callback("Error","CITIESERROR");
                        }
                    }
                })
            },

            //function defined to validate states/cities of businessEnttiy & it's user
            function(callback){

                for(var i=0; i<businessEntitiesArray.length; i++){
                     
                    var errorObject = {};
                    isValidEntity = false;

                    var index1 = states.indexOf(businessEntitiesArray[i].businessEntityDetails.state);
                    var index2 = states.indexOf(businessEntitiesArray[i].businessEntityUsers[0].state);
                    var index3 = cities.indexOf(businessEntitiesArray[i].businessEntityDetails.city);
                    var index4 = cities.indexOf(businessEntitiesArray[i].businessEntityUsers[0].city);

            
                    if(index1>-1 && index2 >-1 && index3 >-1 && index4>-1){
                        isValidEntity = true;
                        validBusinessEntites.push(businessEntitiesArray[i]);
                    }

                    if(!isValidEntity){
                        errorObject.reason = "Invalid state/city";
                        console.log("Invalid state/city");
                        errorObject.failedValue = businessEntitiesArray[i];
                        responseObj.errorResult.push(errorObject);
                    }
                }
                callback(null,'success');
            }

        ],function(err,result){
            //handle error or result

            if(validBusinessEntites.length > 0){
                responseObj.statusCode = 0;
                responseObj.validatedBusinessEntities = validBusinessEntites
            }else{
                responseObj.statusCode = 2;
            }
            next(responseObj)
        })
    },


    /*
    # Method: addBusinessEntitiesMain
    # Description: to add BE and Users to main table in series since same mobile number cannot be added twice
    # Input: 
    # Output : response with success or error
    */

  	addBusinessEntitiesMain:function(businessEntitiesArray,token,responseObj,next){


        var validBusinessEntities = [] ; //array of valid businessEntitiesRecord
        var finalBusinessEntitiesRecord = [];    //final records to be inserted in database
        
        async.series([
            //first function validate state and city
            function(callback){
                console.log("In first function ");
                BusinessEntityBulkUpload.validateBusienssEntitiesStateCity(businessEntitiesArray,responseObj,function(response){

                    if(response.statusCode === 0){
                        validBusinessEntities = response.validatedBusinessEntities;
                        delete responseObj.validatedBusinessEntities;
                        console.log("first function success");
                        callback(null,'success');
                    }
                    else{
                        console.log("first function error");
                        responseObj.message = response.message;
                        callback("Error","Failed to validate state/city");
                    }
                })
            },

            //Second function to remove duplicate mobile number from excelsheet
            function(callback){
                var filterDuplicateMobileNo = {}    //creating an associative array to avoid duplicate mobileNumber
                console.log("before duplicate check",validBusinessEntities.length);
                for(var i=0; i<validBusinessEntities.length; i++){
                    var businessEntity = validBusinessEntities[i];
                    filterDuplicateMobileNo[businessEntity.businessEntityUsers[0].mobileNumber] = businessEntity;
                }

                var k=0;
                for(var businessEntity in filterDuplicateMobileNo){
                    finalBusinessEntitiesRecord[k++] = filterDuplicateMobileNo[businessEntity]
                }

                console.log("after duplicate check",finalBusinessEntitiesRecord.length);

                if(finalBusinessEntitiesRecord.length < validBusinessEntities.length){
                    responseObj.errorMessage = (validBusinessEntities.length - finalBusinessEntitiesRecord.length) + " records failed to insert due to Duplicate Mobile No's!!";
                }

                if(finalBusinessEntitiesRecord.length > 0){
                    callback(null,'success');
                }else{
                    responseObj.message  = "Failed to insert record because of duplicate mobileNo";
                    callback("Error","Error Occurred");
                }
            },

            //function to add state and city in database
            function(callback){
                console.log("Inside second function")
                async.each(finalBusinessEntitiesRecord,function(singleObject,asyncEachCB){
                    var successObj = {};
                    var errorObj = {};
                    var timeStamp = new Date().getTime();
                   
                    singleObject.businessEntityDetails.businessId  = "BE"+timeStamp+businessEntitiesArray.indexOf(singleObject);
                    
                    BusinessEntityMaster.addBusinessEntity(singleObject,token,function(response){
                        console.log("response",response);
                        if (response.statusCode === 0){
                            successObj.SrNo = singleObject.SrNo;
                            successObj.result = response.result;
                            responseObj.successResult.push(successObj);
                            asyncEachCB();
                        }
                        else{
                            errorObj.SrNo = singleObject    .SrNo;
                            errorObj.result = response.result;
                            errorObj.reason = response.message;
                            responseObj.errorResult.push(errorObj);
                            asyncEachCB();
                        }
                    });            
                },
                function(err){
                    console.log("final callback called");
                    if (responseObj.successResult.length>0){
                        responseObj.statusCode = 0;
                        responseObj.message = responseObj.successResult.length + " records inserted successfully." ;
                        if(responseObj.errorResult.length>0){
                            responseObj.message = responseObj.message + responseObj.errorResult.length + " records failed to insert";
                        }

                        callback(null,"success");
                    }
                    else{
                        responseObj.message = "Failed to insert records";
                        callback(null,"success");
                    }   
                                              
                }) // end of async each series
            }],function(err,result){
                
                next(responseObj);
            }
        )

  	},


  	addBusinessEntitiesStaging:function(businessEntitiesArray,token,responseObj,next){

        var validBusinessEntities = [] ; //array of valid businessEntitiesRecord
        var finalBusinessEntitiesRecord = [];    //final records to be inserted in database

        async.series([
            //validaten state/city for businessEntities
            function(callback){
                BusinessEntityBulkUpload.validateBusienssEntitiesStateCity(businessEntitiesArray,responseObj,function(response){

                    if(response.statusCode === 0){
                        //console.log("validateBusinessEntites",response.validatedBusinessEntities);
                        validBusinessEntities = response.validatedBusinessEntities;
                        delete responseObj.validatedBusinessEntities;
                        callback(null)
                    }
                    else{
                        responseObj.message = response.message;
                        callback("Error","Failed to validate state/city");
                    }
                })
            },
            //Addding businessEntities to statging area
            function(callback){
                async.each(businessEntitiesArray,function(singleObject,asyncEachCB2){
                    var successObj = {};
                    var errorObj = {};      
                    var timeStamp = new Date().getTime();
                    //console.log("index is",businessEntitiesArray.indexOf(singleObject));
                    singleObject.businessEntityDetails.businessId  = "BE"+timeStamp+businessEntitiesArray.indexOf(singleObject);

                    BusinessEntityStagingMaster.addBusinessEntity(singleObject,token,function(response){
                        if (response.statusCode === 0){
                            successObj.SrNo = singleObject.SrNo;
                            successObj.result = response.result;
                            responseObj.successResult.push(successObj);
                            asyncEachCB2();
                        }
                        else{
                            errorObj.SrNo = singleObject.SrNo;
                            errorObj.result = response.result;
                            errorObj.reason = response.message;
                            responseObj.errorResult.push(errorObj);
                                asyncEachCB2();
                            }
                        }); // end of add BE
                    },// end of first function
                    function(err){
                        if (responseObj.successResult.length>0){
                            responseObj.statusCode = 0;
                            responseObj.message = responseObj.successResult.length + " records inserted successfully." ;
                            if(responseObj.errorResult.length>0){
                                responseObj.message = responseObj.message + responseObj.errorResult.length + " records failed to insert";
                            }
                        }
                        else{
                            responseObj.message = "Failed to insert records";
                        }
                        callback(null,'success');
                        //next(responseObj);
                }) // end of async each
            }
        ],function(err,result){
            next(responseObj);
        })
    }
};

