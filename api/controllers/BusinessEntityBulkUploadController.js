/**
 * BusinessEntityBulkUploadController
 *
 * @description 	:: Server-side logic for managing Businessentitybulkuploads
 * @help        	:: See http://sailsjs.org/#!/documentation/concepts/Controllers
 * @createdBy       :: Ajeet
 * @created Date    :: 28-01-2017
 */

 

module.exports = {
	
	//Function to add business entity in Bulk
	addBusinessEntityBulk:function(req,res){
		console.log("Inside controller");

		var token = req.token;

		req.connection.setTimeout(30*60*1000); // set timeout of 30 mins

/*		res.setHeader('Content-Type', 'text/html; charset=UTF-8');
		setTimeout(function(){
  				res.write("5 sec ho gaya")
		}, 5000);

		setTimeout(function(){
  				res.write("10 sec ho gaya")
		}, 10000);

		setTimeout(function(){
				res.end();
		}, 13000);
*/
		BusinessEntityBulkUpload.addBusinessEntityBulk(req.body,token,function(response){
			res.json(response);
		});
		
	}

};

