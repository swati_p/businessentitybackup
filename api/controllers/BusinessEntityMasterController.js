/**
 * States.js
 *
 * @description     :: Controller: To add, update and delete the business entities.
 * @createdBy       :: Nilesh
 * @created Date    :: 28-12-2016
 * @Last edited by  :: Nilesh
 * @last edited date::
 */

"use strict";

module.exports = {
	
	//Function to add business entity
	addBusinessEntity:function(req,res){
			
		console.log("inside add BE controller");
		var token = req.token;
		console.log("token is",token);

		var userLogObject = req.body.frontendUserInfo;
		userLogObject.requestApi = req.baseUrl+ req.path;
		userLogObject.event = "Add Business Entity and its users";
		userLogObject.eventType = "Add";


		//Check for KYC done or not
		if(req.body.kycStatus === true){ // If KYC is done then add business entity to BusinessEntityMaster table(primary table)
			if(req.body.existingBusinessEntityUsers.length>0){	// use the Migrate function but dont migrate
				req.body.migrateBE = false;
				BusinessEntityMaster.migrateBusinessEntity(req.body,token,function(response){
					res.json(response);
					// adding user log
					userLogObject.response = response.message;
		  			LogService.addUserLog(userLogObject,token);
				});	
			}
			else{
				BusinessEntityMaster.addBusinessEntity(req.body,token,function(response){
					res.json(response);
					// adding user log
					userLogObject.response = response.message;
		  			LogService.addUserLog(userLogObject,token);					
				});
			}
		}else if(req.body.kycStatus === false){// If KYC is not done then add business entity to BusinessEntityStagingMaster table(secondary table)
			BusinessEntityStagingMaster.addBusinessEntity(req.body,token,function(response){
				res.json(response);
				// adding user log
				userLogObject.response = response.message;
		  		LogService.addUserLog(userLogObject,token);
			});
		}
		else{
			var responseObj = {
				statusCode:-3,
				message:"kycStatus should be either true or false",
				result:null
			};
			res.json(responseObj);
			// adding user log
			userLogObject.response = response.message;
		  	LogService.addUserLog(userLogObject,token);
		}

	},


	//Function to update the business entity
	updateBusinessEntity:function(req,res){

		var token = req.token;
		
		// user log to be added
		var userLogObject = req.body.frontendUserInfo;
		userLogObject.requestApi = req.baseUrl+ req.path;
		userLogObject.event = "Update Business Entity";
		userLogObject.eventType = "Update";

		//Check for KYC done or not and also check whether record is in staging area or not.
		if(req.body.kycStatus === true && req.body.isStaging === false){				//normal update of master table
			BusinessEntityMaster.updateBusinessEntity(req.body,token,function(response){
				res.json(response);
				// adding user log
				userLogObject.response = response.message;
		  		LogService.addUserLog(userLogObject,token);
			});
		}else if(req.body.kycStatus === true && req.body.isStaging === true){          //migration from staging table to master table
			req.body.migrateBE = true;
			BusinessEntityMaster.migrateBusinessEntity(req.body,token,function(response){
				res.json(response);
				// adding user log
				userLogObject.response = response.message;
		  		LogService.addUserLog(userLogObject,token);			
			});
		}else if(req.body.kycStatus === false && req.body.isStaging === true){	       // normal update of staging table
			BusinessEntityStagingMaster.updateBusinessEntity(req.body,token,function(response){
				res.json(response);
				// adding user log
				userLogObject.response = response.message;
		  		LogService.addUserLog(userLogObject,token);				
			});
		}
	},


	//Function to update the business entity
	deleteBusinessEntity:function(req,res){
		var token = req.token;

		var userLogObject = req.body.frontendUserInfo;
		userLogObject.requestApi = req.baseUrl+ req.path;
		userLogObject.event = "Delete Business Entity and its users";
		userLogObject.eventType = "Delete";

		//Check for KYC done or not and also check whether record is in staging area or not.
		if(req.body.kycStatus === true){				//Main table
			//console.log("in deleteBusinessEntity"+req.body+"fn"+BusinessEntityMaster.deleteBusinessEntity);
			BusinessEntityMaster.deleteBusinessEntity(req.body,token,function(response){
				res.json(response);
				// adding user log
				userLogObject.response = response.message;
		  		LogService.addUserLog(userLogObject,token);				
			});
		}
		else if(req.body.kycStatus === false){	       // Staging Table
			console.log("eles if");
			BusinessEntityStagingMaster.deleteBusinessEntity(req.body,token,function(response){
				res.json(response);
				// adding user log
				userLogObject.response = response.message;
		  		LogService.addUserLog(userLogObject,token);				
			});
		}
		else{
			var responseObj = {
				statusCode:-3,
				message:"kycStatus should be either true or false",
				result:null
			};
			res.json(responseObj);
			// adding user log
			userLogObject.response = response.message;
		  	LogService.addUserLog(userLogObject,token);			
		}
	},


	//Function to get all existing Business entity and Business entity user
	getBusinessEntity:function(req,res){

		var token = req.token;
		/*console.log("req ip is",req.ip,"new",req.headers['x-forwarded-for']);
		console.log("req url is",req.baseUrl);
		console.log("req hostname is",req.hostname);
		console.log("req secure is",req.secure);*/


		var userLogObject = req.body.frontendUserInfo;
		userLogObject.requestApi = req.baseUrl+ req.path;
		userLogObject.event = "Get Business Entity and its users";
		userLogObject.eventType = "Get";


		//Check for KYC done or not
		if(req.body.kycStatus === true){ // If KYC is done then fetch from main i.e BusinessEntityMaster table(primary table)
			BusinessEntityMaster.getBusinessEntity(req.body,token,function(response){
				res.json(response);
				// adding user log
				userLogObject.response = response.message;
		  		LogService.addUserLog(userLogObject,token);				
			});
		}else if(req.body.kycStatus === false){// If KYC is not done then fetch from staging i.e BusinessEntityStagingMaster table(secondary table)
			BusinessEntityStagingMaster.getBusinessEntity(req.body,token,function(response){
				res.json(response);
				// adding user log
				userLogObject.response = response.message;
		  		LogService.addUserLog(userLogObject,token);		
			});
		}
		else{
			var responseObj = {
				statusCode:-3,
				message:"kycStatus should be either true or false",
				result:null
			};
			// adding user log
			//userLogObject.response = response.message;
		  	//LogService.addUserLog(userLogObject,token);		
			res.json(responseObj);
		}	
	},

	//Function to find duplicate mobile number
	findDuplicateNumber:function(req,res){

		var token = req.token;

		var userLogObject = req.body.frontendUserInfo;
		userLogObject.requestApi = req.baseUrl+ req.path;
		userLogObject.event = "Find Existing Mobile Number";
		userLogObject.eventType = "Find";

		BusinessEntityUserMaster.findDuplicateNumber(req.body,function(response){
			res.json(response);
			// adding user log
			userLogObject.response = response.message;
		  	LogService.addUserLog(userLogObject,token);		
		});
	},


	//Function to find duplicate mobile number
	updateProgramsMapped:function(req,res){

		var token = req.token;
/*
		var userLogObject = req.body.frontendUserInfo;
		userLogObject.requestApi = req.baseUrl+ req.path;
		userLogObject.event = "Find Existing Mobile Number";
		userLogObject.eventType = "Find";*/

		BusinessEntityMaster.updateProgramsMapped(req.body,token,function(response){
			res.json(response);	
		});
	},

	/**
    *	Method: 		resetApplicationConfigurations
    *	Description: 	controller for /resetApplicationConfigurations API
    *
    */
	resetApplicationConfigurations: function (req, res) {
		// 	Call to loadApplicationConfig method of ServConfigService
		ServConfigService.loadApplicationConfig(function(result){
			var responseObj = {
				"response": "Error",
				"message": null
			};
	    	if(result){
	    		responseObj.response = "Success";
	    		responseObj.message = sails.__('msg_resetApplicationConfigurations_success');
	    		res.json(responseObj);
	    	}else{
	    		responseObj.message = sails.__('msg_resetApplicationConfigurations_fail');
	    		res.json(responseObj);
	    	}
	    });
	}

};

